<div class="top-bar" id="mobile-stick" data-sticky-container>
            <div class="sticky" data-sticky data-margin-top="0">
                <div class="top-bar-title"><span class="menu-toggle" data-responsive-toggle="responsive-menu" data-hide-for="large">
                <button class="menu-icon" type="button" data-toggle="">MENU</button></span>
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logo" title="Michael House">
					<?php if(is_single() || is_category() ):?>

						<?php 
							$classes = get_body_class();
							if( in_array('staff-template-default', $classes) ):?>
							<img src="<?php echo ot_get_option("header_logo"); ?>" alt="Michael's House" class="b-fix"> 
							<img src="<?php echo ot_get_option("sticky_header_logo"); ?>" alt="Michael's House" class=" a-fix"></a>
						<?php else:?>
							<img src="<?php echo ot_get_option("sticky_header_logo"); ?>" alt="Michael's House" class="b-fix"> 
							<img src="<?php echo ot_get_option("sticky_header_logo"); ?>" alt="Michael's House" class=" a-fix"></a>
						<?php endif;?>

					<?php else:?>
						<img src="<?php echo ot_get_option("header_logo"); ?>" alt="Michael's House" class="b-fix"> 
						<img src="<?php echo ot_get_option("sticky_header_logo"); ?>" alt="Michael's House" class=" a-fix"></a>
					<?php endif;?>
                </div>
                <div id="responsive-menu">
                    <div class="top-bar-right is-visible-desktop">
                        <div class="phone-sec bg_red text-center"><p>CALL: </p><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Desktop Header"]'); ?><?php //echo ot_get_option("phone"); ?>
                        </div>
                        <nav class="main-menu">
						<?php wp_nav_menu(array('container' => '', 'menu' => 'top_menu', 'menu_class' => 'menu useful hide-for-small-only clearfix')); ?>
						<?php wp_nav_menu(array('container' => '', 'menu' => 'main_menu', 'menu_class' => 'menu clearfix','theme_location' => 'primary')); ?>
                        </nav>
                    </div>
					<div class="top-bar-right is-visible-mobile">
					<nav class="main-menu">
					
					<ul class="sf-menu menu-mobile" id="example">
                
						 <?php $args = array(
						'order'                  => 'ASC',
						'orderby'                => 'menu_order',
						'post_type'              => 'nav_menu_item',
						'post_status'            => 'publish',
						'output'                 => ARRAY_A,
						'output_key'             => 'menu_order',
						'nopaging'               => true,
						'update_post_term_cache' => false ); ?> 
						<?php $items = wp_get_nav_menu_items( 'main_menu' ); ?>
						<?php foreach ($items as $_menu) : ?>
							
						
							   <?php $parent_id = $_menu->ID; ?> 
								<?php if($_menu->menu_item_parent == 0): $i=0; ?>
								
							<li><a href="<?php echo $_menu->url; ?>"><?php echo $_menu->title; ?></a>
							
							<ul>
								
								<?php foreach ($items as $_menu) :  ?>
								
                            <?php if($_menu->menu_item_parent == $parent_id ): ?>
							
							 <?php $parent_id_1 = $_menu->ID; ?> 
									<li class="submenu_class">
										<a href="<?php echo $_menu->url; ?>"><?php echo $_menu->title; ?></a>
										
										<ul>
										<?php foreach ($items as $_menu) : ?>
										
											<?php if($_menu->menu_item_parent == $parent_id_1 ): ?>
											<li class="submenu_class">
												<a href="<?php echo $_menu->url; ?>"><?php echo $_menu->title; ?></a>
												
											</li>
											<?php endif; ?>
										<?php endforeach; ?>
										</ul>
									</li>
									<?php $i++; endif; ?>
							<?php endforeach; ?>
								</ul>
								
							</li>
							<?php endif;endforeach;?>
							<?php wp_nav_menu(array('container' => '', 'menu' => 'top_menu', 'menu_class' => 'menu useful clearfix')); ?>
						</ul>
					</nav>
					<div class="phone-sec bg_red text-center"> <?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Mobile Menu"]'); ?></a>
					</div>
				</div>
                </div>
            </div>
        </div>