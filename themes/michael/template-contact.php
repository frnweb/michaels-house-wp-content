<?php
/**
* Template Name: Contact Us
*/
get_header();
?>
<?php while ( have_posts() ) : the_post();
$feature_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
if( ! $feature_image ) {
    $feature_image = ot_get_option("default_header_image");
}
?>
<!-- Main Content -->
    <div class="main-content" id="iframeid">
        <!-- Innerpage Banner -->
        <section class="banner banner-innerpage contact mb-0" style="background-image:url(<?php echo $feature_image;?>);">
            <!-- Innerpage Banner Caption -->
            <div class="caption">
                <h1>Contact Us</h1>
               <?php the_content();?>
            </div>
        </section>
        <!-- Banner Ends -->
        <!-- Content Section -->
        <!-- Contact Info Box -->
        <section class="featured">
            <div class="row contact-info-box ">
			<?php if( have_rows('contact_fields') ):  
				  while( have_rows('contact_fields') ): the_row(); 
			?>
                
                <div class="medium-4 columns ">
                    <div class="info-box neg">
                        <figure>
                            <img src="<?php the_sub_field("email_icon");?>" alt="mail-icon-orange">
                        </figure>
                       <?php the_sub_field("email_content");?>
                    </div>
                </div>
				 <?php  endwhile;  endif; ?>
                
            </div>
            <div class="row map-row">
                <div class="medium-5 columns out-vc text-center">
                    <div class="info-box address in-vc text-center">
                        <?php the_field('contact_address');?>
                    </div>
                </div>
                <div class="map-section show-for-small-only">
                    <div class=" flex-video">
                        <?php the_field('map_source_code');?>
                    </div>
                </div>
            </div>
            <div class="map-section hide-for-small-only">
                <div class=" flex-video">
                    <?php the_field('map_source_code');?>
                </div>
            </div>
        </section>
    </div>
<?php endwhile;?>
<?php get_footer();?>