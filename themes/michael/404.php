<?php get_header(); ?>

	<section class="banner banner-innerpage">
        <!-- Innerpage Banner Caption -->
        <div class="caption">
            <h1>Error in Web Address</h1>
    	</div>
    </section>
	
	<div class="row">
        <div class="small-10 small-centered large-centered columns">
			<p>Sorry, we couldn't find a page.</p>
			<?php echo do_shortcode('[frn_related html="404"]'); ?>
			<?php /*
			<p>It looks like something may be wrong with the web address you used.</p>
			<h2>Were you looking for any of these:</h2>
				<?php echo do_shortcode('[frn_related ]'); ?>
			<p><b>...or maybe try searching:</b></p>
			<?php get_search_form(); ?>
			*/ ?>
        </div>
		<div style="margin-bottom:100px;"></div>
		</div>
	</div>


<?php get_footer();
