<?php
/**
 * The template for displaying Category pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
	<!-- Main Content -->
    <div class="main-content">
        <!-- Banner Ends -->
        <!-- Content Section -->
        <div class="row">
            <!-- Left Section -->
            <div class="large-9 small-12 columns">
                <!-- Blog Page Title -->
                <h2 class="pb-20">Michael's House Blog</h2>
                <!-- Blog Article -->
				<?php while ( have_posts() ) : the_post();
				$feature_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>
                <article>
				
                    <div class="row">
					<?php if($feature_image):?>
                        <div class="medium-7 columns">
                           <small><?php $categories = get_the_category();
							if ( ! empty( $categories ) ) {?>
								<a href="<?php echo get_category_link($categories[0]->term_id);?>"><?php echo esc_html( $categories[0]->name );   ?></a><?php
							}?></small>
                            <h1><a href="<?php the_permalink();?>"><?php the_title();?></a></h1>
							<?php the_excerpt();?>
                            <p class="date"><?php echo get_the_date();?></p>
                        </div>
                        <div class="medium-5 columns hide-for-small-only">
                            <figure>
                                <img src="<?php echo $feature_image; ?>" alt="<?php the_title();?>">
                            </figure>
                        </div>
						<?php else:?>
						 <div class="medium-12 columns">
                          <small><?php $categories = get_the_category();
							if ( ! empty( $categories ) ) {?>
								<a href="<?php echo get_category_link($categories[0]->term_id);?>"><?php echo esc_html( $categories[0]->name );   ?></a><?php
							}?></small>
                            <h1><a href="<?php the_permalink();?>"><?php the_title();?></a></h1>
                           <?php the_excerpt();?>
                            <p class="date"><?php echo get_the_date();?></p>
                        </div>
					<?php endif;?>
                    </div>
					
                </article>
				<?php endwhile;?>
               
                <!-- Pagination -->
				<?php if(function_exists('wp_paginate')) {
						wp_paginate();
					}?>
            </div>
            <!-- Sidebar Section -->
            <aside class="large-3  small-12 columns hide-for-small-only">
                 <?php dynamic_sidebar('sidebar-1');?>
            </aside>
        </div>
    </div>
<?php
get_footer();
