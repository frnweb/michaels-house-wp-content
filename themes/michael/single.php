<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<?php while ( have_posts() ) : the_post();
$feature_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
$feature_image_id = get_attachment_id($feature_image); 
$feature_image_meta = wp_get_attachment_metadata($feature_image_id, true);
$opt_feature_image = wp_image_add_srcset_and_sizes($feature_image, $feature_image_meta, $feature_image_id);
?>
 <!-- Main Content -->
    <div class="main-content">
        <!-- Banner Ends -->
        <!-- Content Section -->
        <div class="row">
            <!-- Left Section -->
            <div class="large-9 columns">
            <div id="content">
                <!-- Blog Page Title -->
                <small><span>Blog | </span><?php $categories = get_the_category();
							if ( ! empty( $categories ) ) {?>
								<a href="<?php echo get_category_link($categories[0]->term_id);?>"><?php echo esc_html( $categories[0]->name );   ?></a><?php
							}?></small>
                <h1 class="pb-20"> <?php the_title();?></h1>
                <article>
				<?php if($feature_image):?>
                    <figure class="float-center">
                        <img src="<?php echo $opt_feature_image; ?>" alt="<?php the_title();?>">
                    </figure>
					<?php endif;?>
                    <?php the_content();?>
                    <?php include('page-templates/flexible-content.php'); ?>
                </article>
            </div>
            </div>
            <!-- Sidebar Section -->
            <aside class="large-3 columns">
              <?php dynamic_sidebar('sidebar-1');?>
            </aside>
        </div>
    </div>
<?php endwhile;?>
<?php
get_footer();
