<?php
/*
Template Name: VOB Form
*/
?>
<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post();
$feature_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
if( ! $feature_image ) {
    $feature_image = ot_get_option("default_header_image");
} 
?>
  	<div class="main-content vob-page-template">
        <!-- Innerpage Banner -->
        
        <section class="banner banner-innerpage " style="background-image:url(<?php echo $feature_image;?>);">
            <!-- Innerpage Banner Caption -->
            <div class="caption">
                <h1><?php the_title();?></h1>
            </div>
        </section>
   
        <!-- Banner Ends -->
        <!-- Content Section -->
		<div class="row">
			<div class="large-12 columns">
				<div id="content">
					<?php the_content();?>
					<?php get_template_part( 'parts/content', 'vob-form' ); ?> 
			</div><!-- /.col columns -->
		</div><!-- /.row -->
<?php endwhile; endif; ?>
	</div><!-- end content -->

<?php get_footer(); ?>