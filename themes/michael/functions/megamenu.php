<?php
  
function create_mega_menu( $theme_location ) {
    if ( ($theme_location) && ($locations = get_nav_menu_locations()) && isset($locations[$theme_location]) ) {                      
         
         
        $menu = get_term( $locations[$theme_location], 'nav_menu' );
        $menu_items = wp_get_nav_menu_items($menu->term_id);
 
        $menu_list .= '<ul class="sl_dropdown sl_menu">' ."\n";
          
        foreach( $menu_items as $menu_item ) {

            //If menu item is top level item
            if( $menu_item->menu_item_parent == 0 ) {
                 
                $parent = $menu_item->ID;
                 
                $menu_array = array();

                //classify parent items and create submenu array
                foreach( $menu_items as $submenu ) {
                    if( $submenu->menu_item_parent == $parent ) {
                        $bool = true;
                        $menu_array[] = '<li class="'. implode( "", $submenu->classes ) .'"><a ga-data-menu="' . $menu_item->title . '" href="' . $submenu->url . '">' . $submenu->title . '</a></li>' ."\n";
                    }
                }

                //If the menu item has children
                if( $bool == true && count( $menu_array ) > 0 ) {
                     
                    $menu_list .= '<li data-toggle="sl_megamenu--' . $menu_item->title .'">' ."\n";
                    $menu_list .= '<a ga-data-menu="' . $menu_item->title . '" href="' . $menu_item->url . '">' . $menu_item->title . '</a>' ."\n";
                     
                    $menu_list .= '<div id="sl_megamenu--' . $menu_item->title .'"" class="sl_dropdown-pane sl_megamenu sl_megamenu--' . $menu_item->title .'" ga-data-menu="' . $menu_item->title . '" data-dropdown data-options="closeOnClick:true; hover: true; hoverPane: true;">' ."\n";
                    

                    //TREATMENT MEGA MENU
                    if( $menu_item->title == 'Treatment') {
                        $menu_list .= '<div class="sl_row">' . "\n";

                        //create treatment submenu array
                        $submenu_array = array();
                        foreach( $menu_items as $submenu ) {
                            if ( $submenu->menu_item_parent == $parent ) {
                                $submenu_array[] = $submenu;
                            }             
                        }//end creating submenu

                        // Locations
                        foreach ($submenu_array as $key => $submenu_item) {
                            //Create locations menu array
                            $locations_array = array();
                            $submenu_parent = $submenu_item->ID; 
                            foreach ( $menu_items as $locations ) {
                                if( $locations->menu_item_parent == $submenu_parent ) {
                                    $bool = true;
                                    $locations_array[] = $locations;
                                }
                            }//end create locations menu array
                            if ( $key < 1) continue;
                            if ( $key == 1 ) {
                                $menu_list .= '<div class="sl_cell--' . $key . ' sl_cell">' . "\n";
                                $menu_list .= '<h2><a ga-data-menu="' . $menu_item->title . '" href="' . $submenu_item->url . '">' . $submenu_item->title . '</a></h2>' . "\n";
                                $menu_list .= '<div class="sl_row">' ."\n";

                                foreach ($locations_array as $key => $locations_item) {
                                    $menu_list .= '<div class="sl_cell--' . $key . ' sl_cell">' . "\n";
                                    $menu_list .= '<div class="sl_megamenu--'. $menu_item->title .'__image"></div>' . "\n";
                                    $menu_list .= '<a ga-data-menu="' . $menu_item->title . '" href="' . $locations_item->url . '">' . $locations_item->title . '</a>' ."\n";
                                    $menu_list .= '</div>' . "\n";// closing sl_cell
                                }

                                $menu_list .= '</div>' ."\n";// closing sl_row
                                $menu_list .= '</div>' . "\n";// closing sl_cell
                            }
                        }//end Locations

                        //Default Layout if not Locations
                        foreach ($submenu_array as $key => $submenu_item) {
                            //Create grandchild menu array
                            $grandchild_array = array();
                            $submenu_parent = $submenu_item->ID; 
                            foreach ( $menu_items as $grandchild ) {
                                if( $grandchild->menu_item_parent == $submenu_parent ) {
                                    $bool = true;
                                    $grandchild_array[] = '<li class="'. implode( "", $grandchild->classes ) .'"><a ga-data-menu="' . $menu_item->title . '" href="' . $grandchild->url . '">' . $grandchild->title . '</a></li>' ."\n";
                                }
                            }//end create grandchild menu array

                            if ( $key > 1 ){
                                $menu_list .= '<div class="sl_cell--' . $key . ' sl_cell">' . "\n";
                                $menu_list .= '<h2><a ga-data-menu="' . $menu_item->title . '" href="' . $submenu_item->url . '">' . $submenu_item->title . '</a></h2>' . "\n";
                                $menu_list .= '<ul class="sl_menu">' ."\n";
                                $menu_list .= implode( "\n", $grandchild_array );
                                $menu_list .= '</ul>' ."\n";
                                $menu_list .= '</div>' . "\n";// closing sl_cell
                            }
                        }//end default layout

                        $menu_list .= '</div>' ."\n";// closing sl_row
                    }//END TREATMENT MEGA MENU

                    //RESOURCES MEGA MENU
                    elseif( $menu_item->title == 'Resources') {
                        $menu_list .= '<div class="sl_row">' . "\n";

                        //create resources submenu array
                        $submenu_array = array();
                        foreach( $menu_items as $submenu ) {
                            if ( $submenu->menu_item_parent == $parent ) {
                                $submenu_array[] = $submenu;
                            }             
                        }//end creating submenu

                        foreach ($submenu_array as $key => $submenu_item) {
                            //Create grandchild menu array
                            $grandchild_array = array();
                            $submenu_parent = $submenu_item->ID; 
                            foreach ( $menu_items as $grandchild ) {
                                if( $grandchild->menu_item_parent == $submenu_parent ) {
                                    $bool = true;
                                    $grandchild_array[] = '<li class="'. implode( "", $grandchild->classes ) .'"><a ga-data-menu="' . $menu_item->title . '"  ga-data-menu="' . $menu_item->title . '" href="' . $grandchild->url . '">' . $grandchild->title . '</a></li>' ."\n";
                                }
                            }//end create grandchild menu array

                            if ( $key < 1 ) continue;
                            //First Two Items
                            if ( $key < 3 ) {
                                $menu_list .= '<div class="sl_cell--' . $key . ' sl_cell">' . "\n";
                                $menu_list .= '<h2><a ga-data-menu="' . $menu_item->title . '" href="' . $submenu_item->url . '">' . $submenu_item->title . '</a></h2>' . "\n";
                                $menu_list .= '<ul class="sl_menu">' ."\n";
                                $menu_list .= implode( "\n", $grandchild_array );
                                $menu_list .= '</ul>' ."\n";
                                $menu_list .= '<a ga-data-menu="' . $menu_item->title . '" class="sl_button sl_button--icon" href="' . $submenu_item->url . '">All Topics</a>' ."\n";
                                $menu_list .= '</div>' . "\n";// closing sl_cell
                            }

                            elseif ( $key == 3 ) {
                                $menu_list .= '<div class="sl_cell--' . $key . ' sl_cell">' . "\n";
                                $menu_list .= '<h2><a ga-data-menu="' . $menu_item->title . '" href="' . $submenu_item->url . '">' . $submenu_item->title . '</a></h2>' . "\n";
                                $menu_list .= '<ul class="sl_menu">' ."\n";
                                $menu_list .= implode( "\n", $grandchild_array );
                                $menu_list .= '</ul>' ."\n";
                                $menu_list .= '</div>' . "\n";// closing sl_cell
                            }
                        }

                        //Default Layout if not the first 3 items
                        $menu_list .= '<div class="sl_cell">' . "\n";
                        $menu_list .= '<ul class="sl_menu">' ."\n";
                        foreach ($submenu_array as $key => $submenu_item) {
                            if ( $key > 3 ){
                                $menu_list .= '<li class="'. implode( "", $submenu_item->classes ) .'"><a ga-data-menu="' . $menu_item->title . '" href="' . $submenu_item->url . '">' . $submenu_item->title . '</a></li>' ."\n";
                            }
                        }
                        $menu_list .= '</ul>' ."\n";
                        $menu_list .= '</div>' ."\n";// closing sl_cell
                        $menu_list .= '</div>' ."\n";// closing sl_row
                    }//END RESOURCES MEGA MENU

                    //DEFAULT MEGA MENU MARKUP
                    else {
                        $menu_list .= '<div class="sl_row">' . "\n";

                        //Left Mega Menu
                        $menu_list .= '<div class="sl_cell">' . "\n";
                        $menu_list .= '<div class="sl_megamenu__image"></div>' . "\n"; // Mega Menu Image
                        $menu_list .= '<div class="sl_megamenu__content">' . "\n";
                        $menu_list .= '<h2><a ga-data-menu="' . $menu_item->title . '" href="' . $menu_item->url . '">' . $menu_item->title . '</a></h2>' . "\n";

                        //Add Parent Description if Exists
                        if( $menu_item->description != null ) {
                            $menu_list .= '<p>' . $menu_item->description . '</p>' . "\n";
                        }

                        $menu_list .= '<a ga-data-menu="' . $menu_item->title . '" class="sl_button" href="' . $menu_item->url . '">Learn More</a>' ."\n";
                        $menu_list .= '</div>' ."\n";// closing sl_megamenu__content
                        $menu_list .= '</div>' ."\n";// closing sl_cell

                        //Right Mega Menu
                        $menu_list .= '<div class="sl_cell">' . "\n";
                        $menu_list .= '<ul class="sl_menu">' ."\n";
                        $menu_list .= implode( "\n", $menu_array );
                        $menu_list .= '</ul>' ."\n";
                        $menu_list .= '</div>' ."\n";// closing sl_cell

                        $menu_list .= '</div>' ."\n";// closing sl_row
                        $menu_list .= '</div>' ."\n";// closing sl_mega
                    }//end default mega menu

                    
                     
                }//end conditional for children

                //If Menu item has no children
                else {
                     
                    $menu_list .= '<li>' ."\n";
                    $menu_list .= '<a ga-data-menu="' . $menu_item->title . '" href="' . $menu_item->url . '">' . $menu_item->title . '</a>' ."\n";
                }//end conditional for no children
                 
            }//end conditional for top level item
             
            // end <li>
            $menu_list .= '</li>' ."\n";

        }//end menu loop
          
        $menu_list .= '</ul>' ."\n";
  
    }//end conditional for theme menu location
     
    echo $menu_list;
}