<?php
  
function create_offcanvas_menu( $theme_location ) {
    if ( ($theme_location) && ($locations = get_nav_menu_locations()) && isset($locations[$theme_location]) ) {                      
         
         
        $menu = get_term( $locations[$theme_location], 'nav_menu' );
        $menu_items = wp_get_nav_menu_items($menu->term_id);
 
        $menu_list .= '<ul class="sl_menu sl_accordion-menu" data-accordion-menu>' ."\n";
          
        foreach( $menu_items as $menu_item ) {

            //If menu item is top level item
            if( $menu_item->menu_item_parent == 0 ) {
                 
                $parent = $menu_item->ID;
                 
                $menu_array = array();

                //classify parent items and create submenu array
                foreach( $menu_items as $submenu ) {

                    if( $submenu->menu_item_parent == $parent ) {
                        $bool = true;
                        $menu_array[] = $submenu;
                    }
                }

                //If the menu item has children
                if( $bool == true && count( $menu_array ) > 0 ) {
                     
                    $menu_list .= '<li>' ."\n";
                    $menu_list .= '<a href="' . $menu_item->url . '">' . $menu_item->title . '</a>' ."\n"; 
                    $menu_list .= '<ul class="sl_menu sl_nested">' ."\n";
                    
                    foreach ($menu_array as $submenu ) {

                        //Creating grandchild array
                        $submenu_array = array();
                        $subparent = $submenu->ID; 
                        foreach( $menu_items as $grandchild ) {
                            if( $grandchild->menu_item_parent == $subparent ) {
                                $subbool = true;
                                $submenu_array[] = '<li><a ga-data-menu="' . $menu_item->title . '" href="' . $grandchild->url . '">' . $grandchild->title . '</a></li>' ."\n";
                            }
                        }

                        //If submenu has children
                        if( $subbool == true && count( $submenu_array ) > 0 ) {
                            $menu_list .= '<li>' ."\n";
                            $menu_list .= '<a ga-data-menu="' . $menu_item->title . '" href="' . $submenu->url . '">' . $submenu->title . '</a>' ."\n";
                            $menu_list .= '<ul class="sl_menu sl_nested">' ."\n";
                            $menu_list .= implode( "\n", $submenu_array );
                            $menu_list .= '</ul>' ."\n";
                        }
                        else {
                            $menu_list .= '<li>' ."\n";
                            $menu_list .= '<a ga-data-menu="' . $menu_item->title . '" href="' . $submenu->url . '">' . $submenu->title . '</a>' ."\n";
                        }
                        $menu_list .= '</li>' ."\n";
                    }
                    $menu_list .= '</ul>' ."\n";
  
                }//End if has children

                //If Menu item has no children
                else {
                    $menu_list .= '<li>' ."\n";
                    $menu_list .= '<a href="' . $menu_item->url . '">' . $menu_item->title . '</a>' ."\n";
                }//end conditional for no children
            }//end conditional for top level item

            $menu_list .= '</li>' ."\n";
        }//end menu loop
          
        $menu_list .= '</ul>' ."\n";
    }//end conditional for theme menu location
     
    echo $menu_list;
}