<div id="sl_offcanvas" class="sl_mobilemenu off-canvas position-left" data-off-canvas>

	<!-- Close Button -->
	<button class="sl_mobilemenu__close" type="button" aria-label="Close Menu" data-close>
		<span class="fa fa-times"></span>
	</button>

	<!-- Logo -->
	<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="sl_mobilemenu__logo">
		<img src="<?php echo ot_get_option("header_logo"); ?>" alt="Michael's House"> 
	</a><!-- /.sl_nav__logo -->

	<!-- Menu -->
	<?php create_offcanvas_menu("primary"); ?>

	<!-- Callout -->
	<div class="sl_mobilemenu__callout">
		<h4>Have questions? Get help now.</h4>
		<?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks Mobile Menu" class="sl_button" text="Call Us"]'); ?>
		<?php echo do_shortcode('[mhouse_sms class="sl_button sl_button--secondary" text="Text Us"]'); ?>
	</div><!-- /.sl_mobilemenu__callout -->
</div><!-- /#sl_mobilemenu -->