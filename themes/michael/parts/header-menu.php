<!-- MOBILE MENU -->
<div class="sl_mobilenav" data-responsive-toggle="sl_mobilemenu" data-hide-for="large">
	
	<!-- Left Side -->
	<div class="sl_mobilenav__left">
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="sl_mobilenav__logo">
			<img src="<?php echo ot_get_option("header_logo"); ?>" alt="Michael's House"> 
		</a><!-- /.sl_nav-logo -->
	</div><!-- /.sl_mobilenav__left -->

	<!-- Right Side -->
	<div class="sl_mobilenav__right">
		<button class="sl_button sl_button--menu" type="button" data-toggle="sl_offcanvas">
			<span class="sl_mobilenav__label">Menu</span>
		</button><!-- /.sl_button sl_button--menu -->
	</div><!-- /.sl_mobilenav__right -->
</div><!-- sl_mobilnav -->


<!-- DESKTOP MENU -->
<div class="sl_nav show-for-large">

		<!-- Left Side -->
		<div class="sl_nav__left">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="sl_nav__logo">
				<img src="<?php echo ot_get_option("header_logo"); ?>" alt="Michael's House"> 
			</a><!-- /.sl_nav__logo -->
		</div><!-- /.sl_nav__left -->

		<!-- Right Side -->
		<div class="sl_nav__right">
			<div class="sl_nav__menu">
				<?php
				create_mega_menu("primary")
				?>
			</div><!-- /.sl_nav__menu -->

			<div class="sl_nav__phone">
				<div class="sl_phone">
					<span class="sl_phone__cta">Call:</span>
					<?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in the Header" class="sl_button sl_button--phone sl_phone__number"]')?>
				</div> <!-- /.sl_phone -->
			</div> <!-- /.sl_nav__phone -->
		</div><!-- /.sl_nav__right -->
</div><!-- /.sl_nav -->