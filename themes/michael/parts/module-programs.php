<!-- Tabs for Medium and Up -->
<section class="sl_module sl_module--duo sl_duo sl_duo--tabs show-for-medium">
    <div class="sl_inner">
        <h2>Programs Individualized to Meet Your Needs</h2>
        <ul class="tabs sl_tabs" data-tabs id="sl_program_tabs">
            <li class="tabs-title sl_tabs__title is-active"><a href="#panel1" aria-selected="true">Men's Program</a></li>
            <li class="tabs-title sl_tabs__title"><a data-tabs-target="panel2" href="#panel2">Women's Program</a></li>
            <li class="tabs-title sl_tabs__title"><a data-tabs-target="panel3" href="#panel3">LGBTQ+ Informed Services</a></li>
            <li class="tabs-title sl_tabs__title"><a data-tabs-target="panel4" href="#panel4">Adventure Therapy</a></li>
        </ul>
        <div class="tabs-content sl_tabs__content" data-tabs-content="sl_program_tabs">
            <div class="sl_tabs__panel tabs-panel is-active" id="panel1">
                <div class="sl_row">
                    <div class="sl_duo__content sl_cell medium-6">
                        <div class="sl_card">
                            <h2>Men's Program</h2>
                            <p>In the Men’s Program individuals will learn how to process and problem-solve, recognize positive and negative patterns, build character, and find balance along the way. Our experienced staff helps patients find stability and build an arsenal of healthy coping skills to help them overcome life challenges ahead.</p>
                            <a class="sl_button sl_button--simple" href="/our-programs/mens-program/">Is our Men's Program right for you?</a>
                        </div><!-- sl_card -->
                    </div><!-- sl_duo__content -->
                    <div class="sl_duo__media sl_cell medium-6">
                        <div class="sl_duo__media__container">
                            <div class="sl_duo__image" title="Michael's House Courtyard Fountain" style="background-image: url(/wp-content/themes/michael/images/assets/MH-Fountain-Courtyard.jpg)"></div>
                        </div><!-- sl_duo__media__container -->
                    </div><!-- sl_duo__media -->
                </div><!-- sl_row -->
            </div><!-- sl_tabs__panel -->
            <div class="sl_tabs__panel tabs-panel" id="panel2">
                <div class="sl_row">
                    <div class="sl_duo__content sl_cell medium-6">
                        <div class="sl_card">
                            <h2>Women's Program</h2>
                            <p>The Women’s Program builds on each patient’s individual strengths. Patients learn new and healthy ways to set boundaries, problem-solve, reinforce positive mindsets, and find balance. The Women’s Program team is devoted to the well-being of each individual, consistently collaborating to discuss progress and the best options for hope and healing.</p>
                            <a class="sl_button sl_button--simple" href="/our-programs/womens-program/">Is our Women's Program right for you?</a>
                        </div><!-- sl_card -->
                    </div><!-- sl_duo__content -->
                    <div class="sl_duo__media sl_cell medium-6">
                        <div class="sl_duo__media__container">
                            <div class="sl_duo__image" title="Michael's House Entrance" style="background-image: url(/wp-content/themes/michael/images/assets/MH-Entrance.jpg)"></div>
                        </div><!-- sl_duo__media__container -->
                    </div><!-- sl_duo__media -->
                </div><!-- sl_row -->
            </div><!-- sl_tabs__panel -->
            <div class="sl_tabs__panel tabs-panel" id="panel3">
                <div class="sl_row">
                    <div class="sl_duo__content sl_cell medium-6">
                        <div class="sl_card">
                            <h2>LGBTQ+ Informed Services</h2>
                            <p>Michael’s House offers members of the LGBTQ+ community specific process and psychoeducational groups, as well as primary therapists trained in LGBTQ+ issues. We provide a safe place to address both the external and internalized oppression that can often present barriers to an LGBTQ+ individual’s recovery from substance use and mental health disorders.</p>
                            <a class="sl_button sl_button--simple" href="/our-programs/lgbt-program-michaels-house/">Is our LGBTQ+ Informed Services right for you?</a>
                        </div><!-- sl_card -->
                    </div><!-- sl_duo__content -->
                    <div class="sl_duo__media sl_cell medium-6">
                        <div class="sl_duo__media__container">
                            <div class="sl_duo__image" title="Michael's House Treatment Center Entrance" style="background-image: url(/wp-content/themes/michael/images/assets/MH-Treatment-Entrance.jpg)"></div>
                        </div><!-- sl_duo__media__container -->
                    </div><!-- sl_duo__media -->
                </div><!-- sl_row -->
            </div><!-- sl_tabs__panel -->
            <div class="sl_tabs__panel tabs-panel" id="panel4">
                <div class="sl_row">   
                    <div class="sl_duo__content sl_cell medium-6">
                        <div class="sl_card">
                            <h2>Adventure Therapy</h2>
                            <p>Adventure therapy offers patients the opportunity to develop important life skills by exploring personal issues beyond the walls of the treatment center in a safe, supportive environment under the guidance and support of trained professionals. The program starts out with light holistic therapies and can grow into advanced hikes, biking, and even rock climbing!</p>
                            <a class="sl_button sl_button--simple" href="/adventure-program/">Is our Adventure Therapy right for you?</a>
                        </div><!-- sl_card -->
                    </div><!-- sl_duo__content -->
                    <div class="sl_duo__media sl_cell medium-6">
                        <div class="sl_duo__media__container">
                            <div class="sl_duo__image" title="Michael's House Courtyard Fountain" style="background-image: url(/wp-content/themes/michael/images/assets/MH-Fountain-Courtyard.jpg)"></div>
                        </div><!-- sl_duo__media__container -->
                    </div><!-- sl_duo__media -->
                </div><!-- sl_row -->
            </div><!-- sl_tabs__panel -->
        </div><!-- sl_tabs__content -->
    </div><!-- sl_inner -->
</section>

<!-- Carousel for Small -->
<section class="sl_module sl_module--carousel sl_carousel show-for-small-only">
    <div class="sl_inner sl_inner--expanded">
        <h2>Programs Individualized to Meet Your Needs</h2>
        <div class="sl_program__slider">
            <div>
                <div data-open="sl_mens-modal" class="sl_program__slider__slide" style="background-image:url('wp-content/themes/michael/images/assets/MH-Entrance.jpg')">
                    <h3>Men's Program</h3>
                </div>
            </div>
            <div>
                <div data-open="sl_womens-modal" class="sl_program__slider__slide" style="background-image:url('wp-content/themes/michael/images/assets/MH-Fountain-Courtyard.jpg')">
                    <h3>Women's Program</h3>
                </div>
            </div>
            <div>
                <div data-open="sl_lgbtq-modal" class="sl_program__slider__slide" style="background-image:url('wp-content/themes/michael/images/assets/MH-Treatment-Entrance.jpg')">
                    <h3>LGBTQ+ Informed Services</h3>
                </div>
            </div>
            <div>
                <div data-open="sl_adventure-modal" class="sl_program__slider__slide" style="background-image:url('wp-content/themes/michael/images/assets/MH-Entrance.jpg')">
                    <h3>Adventure Therapy</h3>
                </div>
            </div>
        </div><!--/.sl_program__slider-->
    </div><!-- sl_inner -->

    <div id="sl_mens-modal" class="sl_reveal reveal" data-reveal>
        <h2>Men's Program</h2>
        <p>In the Men’s Program individuals will learn how to process and problem-solve, recognize positive and negative patterns, build character, and find balance along the way. Our experienced staff helps patients find stability and build an arsenal of healthy coping skills to help them overcome life challenges ahead.</p>
        <a class="sl_button sl_button--simple" href="/our-programs/mens-program/">Is our Men's Program right for you?</a>
        <button class="close-button" data-close aria-label="Close modal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
    </div><!-- /#sl_mens-modal -->

    <div id="sl_womens-modal" class="sl_reveal reveal" data-reveal>
        <h2>Women's Program</h2>
        <p>The Women’s Program builds on each patient’s individual strengths. Patients learn new and healthy ways to set boundaries, problem-solve, reinforce positive mindsets, and find balance. The Women’s Program team is devoted to the well-being of each individual, consistently collaborating to discuss progress and the best options for hope and healing.</p>
        <a class="sl_button sl_button--simple" href="/our-programs/womens-program/">Is our Women's Program right for you?</a>
        <button class="close-button" data-close aria-label="Close modal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
    </div><!-- /#sl_womens-modal -->

    <div id="sl_lgbtq-modal" class="sl_reveal reveal" data-reveal>
        <h2>LGBTQ+ Informed Services</h2>
        <p>Michael’s House offers members of the LGBTQ+ community LGBTQ+ specific process groups, primary therapists trained in LGBTQ+ issues, and LGBTQ+ psychoeducational groups. We provide a safe place to address both the external and internalized oppression that can often present barriers to an LGBTQ+ individual’s recovery from substance use and mental health disorders.</p>
        <a class="sl_button sl_button--simple" href="/our-programs/lgbt-program-michaels-house/">Is our LGBTQ+ Informed Services right for you?</a>
        <button class="close-button" data-close aria-label="Close modal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
    </div><!-- /#sl_lgbtq-modla -->

    <div id="sl_adventure-modal" class="sl_reveal reveal" data-reveal>
        <h2>Adventure Therapy</h2>
        <p>Adventure therapy offers patients the opportunity to develop important life skills by exploring personal issues beyond the walls of the treatment center in a safe, supportive environment under the guidance and support of trained professionals. The program starts out with light holistic therapies and can grow into advanced hikes, biking, and even rock climbing!</p>
        <a class="sl_button sl_button--simple" href="/adventure-program/">Is our Adventure Therapy right for you?</a>
        <button class="close-button" data-close aria-label="Close modal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
    </div><!-- /#sl_adventure-modal -->
</section>