        <div id="vob-popup" class="popup-card" data-reveal data-reset-on-close="true" data-close-on-click="true" data-animation-in="fade-in" data-animation-out="fade-out">
            <div class="row">
                <div class="large-7 medium-12 columns show-for-large">
                    <div class="card">
                        <h1>Free Insurance Verification</h1>
                        <div class="divider"></div>
                        <h3>Some of the many insurance providers we work with:</h3>
                        <div class="insurance-container">
                            <div class="row medium-up-3">
                                <div class="columns">
                                    <img src="<?php bloginfo('stylesheet_directory'); ?>/images/insurance-logos/bluecrossblueshield.png">
                                </div><!--/.columns-->
                                <div class="columns">
                                     <img src="<?php bloginfo('stylesheet_directory'); ?>/images/insurance-logos/aetna.png">
                                </div><!--/.columns-->
                                <div class="columns">
                                     <img src="<?php bloginfo('stylesheet_directory'); ?>/images/insurance-logos/humana.png">
                                </div><!--/.columns-->
                                <div class="columns">
                                     <img src="<?php bloginfo('stylesheet_directory'); ?>/images/insurance-logos/magellan.png">
                                </div><!--/.columns-->
                                <div class="columns">
                                     <img src="<?php bloginfo('stylesheet_directory'); ?>/images/insurance-logos/valueoptions.png">
                                </div><!--/.columns-->
                                <div class="columns">
                                     <img src="<?php bloginfo('stylesheet_directory'); ?>/images/insurance-logos/coresource.png">
                                </div><!--/.columns-->
                                <div class="columns">
                                     <img src="<?php bloginfo('stylesheet_directory'); ?>/images/insurance-logos/tricare.png">
                                </div><!--/.columns-->
                            </div><!--/.row.medium-up-3-->
                        </div><!--/.insurance-container-->
                    </div><!--/.card-->
                </div><!--/.medium-7.columns-->
                <div class="large-5 medium-12 columns">
                    <div class="card">
                        <h2>Verify Your Benefits</h2>
                        <p>We will help you explore what options your policy covers and work hard to help you get the coverage you need.</p>
                        <a class="button primary" href="<?php echo home_url('/'); ?>verify-your-benefits" onclick="ga('send', 'event', 'VOB Popup', 'Get Started', 'VOB Form Launch');">Get Started</a>
                        <p class="phone">or call <?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in VOB Popup"]'); ?></p>                           
                    </div><!--/.card-->
                    <p class="privacy-policy">Confidential & Private | <a href="/wp-content/plugins/frn_plugins/privacy-policy.pdf">Privacy Policy</a></p>
                </div><!--/.medium-5.columns-->
            </div><!--/.row-->
            <button class="close-button" data-close aria-label="Close modal" type="button" onclick="ga('send', 'event', 'VOB Popup', 'Close Popup', 'VOB Form Launch');">
                <span aria-hidden>&times;</span>
            </button><!--/button-->
        </div><!--/.popup-card-->

<!-- VOB Form Pop Up Ends -->