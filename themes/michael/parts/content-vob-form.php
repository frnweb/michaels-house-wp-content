<script src="https://www.google.com/recaptcha/api.js"></script>
<script>
        function timestamp() {
            var response = document.getElementById("g-recaptcha-response");
            if (response == null || response.value.trim() == "") {
                var elems = JSON.parse(document.getElementsByName("captcha_settings")[0].value);
                elems["ts"] = JSON.stringify(new Date().getTime());
                document.getElementsByName("captcha_settings")[0].value = JSON.stringify(elems);
            }
        } setInterval(timestamp, 500);
        
function recaptcha_callback(){
    jQuery('.button--secure').prop("disabled", false);
}
</script>

<div class="grid-container full" id="vob-holder">
    <form action="https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="POST">
        <input type="hidden" name='captcha_settings' value='{"keyname":"FRNCaptcha","fallback":"true","orgId":"00Dj0000001rMuh","ts":""}'/> 
        <input type="hidden" name="oid" value="00Dj0000001rMuh">
        <input type="hidden" name="retURL" value="https://www.michaelshouse.com/vob-thank-you/">

        <div class="inner">
            <h3>Let’s Get You Some Answers</h3>
            <p> By requesting a <strong>risk-free, confidential assessment</strong> we promise to guide you in finding the best solution for your individual needs.</p>
            <div class="row grid-margin-x">
                <div class="large-12 medium-12 columns">
                    <h5>Who are you seeking help for today?</h5>
                </div><!-- end columns --> 

                <div class="large-6 medium-6 columns end">
                    <select id="00Nj000000BKJje" name="00Nj000000BKJje" title="Calling for" required>
                        <option value="" disabled="" selected="">Select One</option>
                        <option value="Self">Myself</option>
                        <option value="Loved One">Loved One</option>
                        <option value="Client/Patient">Client/Patient</option>
                    </select><!-- end calling for dropdown-->
                </div><!-- end columns -->
            </div> <!---end row --> 

            <div class="row grid-margin-x">
                <div class="large-12 medium-12 columns">
                    <h5>Is the person in need of help 18 or older?</h5>
                    <input id="00N0a00000CVU7J" name="00N0a00000CVU7J" type="checkbox" value="1"><label for="00N0a00000CVU7J">Yes</label>
                    <input id="under_eighteen" name="under_eighteen" type="checkbox" value="No"><label for="under_eighteen">No</label>
                </div><!-- end columns -->
            </div><!---end row --> <!-- end section about who they are reaching out for--> 

            <hr>

            <div class="row grid-margin-x">
                <div class="large-6 medium-6 columns">
                    <label for="first_name"><h5><span class="patient-reveal">Patient</span> First Name</h5></label>
                    <input id="first_name" maxlength="40" name="first_name" type="text" required>
                </div><!-- end columns for patient first name -->

                <div class="large-6 medium-6 columns">
                    <label for="last_name"><h5><span class="patient-reveal">Patient</span> Last Name</h5></label>
                    <input id="last_name" maxlength="80" name="last_name" type="text" required>
                </div><!-- end columns for patient last name-->
            </div><!-- end grid row -->

            <div id="caller-contact" class="patient-reveal">
                <div class="row grid-margin-x">
                    <div class="large-6 medium-6 columns">
                        <label for="00Nj000000BKJjY"><h5>Contact First Name</h5></label>
                        <input id="00Nj000000BKJjY" maxlength="72" name="00Nj000000BKJjY" type="text">
                    </div><!-- end columns for Contact first name-->

                    <div class="large-6 medium-6 columns">
                        <label for="00Nj000000BKJja"><h5>Contact Last Name</h5></label>
                        <input id="00Nj000000BKJja" maxlength="72" name="00Nj000000BKJja" type="text"><!-- caller last name-->
                    </div><!-- end large-12-->
                </div><!-- end grid row -->
            </div><!-- end caller-contact -->

            <div class="row grid-margin-x">
                <div class="large-6 medium-6 columns">
                    <label for="phone"><h5><span class="contact-reveal">Contact</span> Phone Number</h5></label>
                    <input id="phone" maxlength="40" name="phone" type="text" placeholder="XXX-XXX-XXXX" required>
                </div><!-- contact phone number-->

                <div class="large-6 medium-6 columns">
                    <label for="email"><h5><span class="contact-reveal">Contact</span> Email <span class="optional">- Optional</span></h5></label>
                    <input id="email" maxlength="80" name="email" type="text">
                </div><!-- end email-->
            </div><!-- end grid row -->

            <div class="row grid-margin-x">
                <div class="large-6 medium-6 columns">
                    <label for="city"><h5><span class="patient-reveal">Patient</span> City</h5></label><input id="city" maxlength="40" name="city" size="20" type="text" required>
                </div><!-- end columns for patient city-->

                <div class="large-6 medium-6 columns">
                    <label for="state_code"><h5><span class="patient-reveal">Patient</span> State</h5></label>
                    <select id="state_code" name="state_code" required>
                        <option value="AL">AL</option>
                        <option value="AK">AK</option>
                        <option value="AZ">AZ</option>
                        <option value="AR">AR</option>
                        <option value="CA">CA</option>
                        <option value="CO">CO</option>
                        <option value="CT">CT</option>
                        <option value="DE">DE</option>
                        <option value="FL">FL</option>
                        <option value="GA">GA</option>
                        <option value="HI">HI</option>
                        <option value="ID">ID</option>
                        <option value="IL">IL</option>
                        <option value="IN">IN</option>
                        <option value="IA">IA</option>
                        <option value="KS">KS</option>
                        <option value="KY">KY</option>
                        <option value="KY">LA</option>
                        <option value="ME">ME</option>
                        <option value="MD">MD</option>
                        <option value="MA">MA</option>
                        <option value="MI">MI</option>
                        <option value="MN">MN</option>
                        <option value="MS">MS</option>
                        <option value="MO">MO</option>
                        <option value="MT">MT</option>
                        <option value="NE">NE</option>
                        <option value="NV">NV</option>
                        <option value="NH">NH</option>
                        <option value="NJ">NJ</option>
                        <option value="NM">NM</option>
                        <option value="NY">NY</option>
                        <option value="NC">NC</option>
                        <option value="ND">ND</option>
                        <option value="NO">NO</option>
                        <option value="OH">OH</option>
                        <option value="OK">OK</option>
                        <option value="OR">OR</option>
                        <option value="PA">PA</option>
                        <option value="RI">RI</option>
                        <option value="SC">SC</option>
                        <option value="SD">SD</option>
                        <option value="TN">TN</option>
                        <option value="TX">TX</option>
                        <option value="UT">UT</option>
                        <option value="VA">VA</option>
                        <option value="VT">VT</option>
                        <option value="WA">WA</option>
                        <option value="WV">WV</option>
                        <option value="WI">WI</option>
                        <option value="WY">WY</option>
                    </select><!-- end drop down for states -->
                </div><!-- end columns for state dropdown-->
            </div><!-- end grid row for patient state and city-->

            <div class="row grid-margin-x">
                <div class="large-12 medium-12 columns">
                    <label for="00N0a00000CVU7I"><h5><span class="patient-reveal">Patient</span> Insurance Type</h5></label>
                    <select id="00N0a00000CVU7I" name="00N0a00000CVU7I" title="Insurance Type" required>
                        <option value="" disabled="" selected="">View Options</option>
                        <option value="Employer Provided/Individually Provided">Employer Provided/Individually Provided</option>
                        <option value="Government Provided (Medicaid)">Government Provided (Medicaid)</option>
                        <option value="Government Provided (Medicare)">Government Provided (Medicare)</option>
                        <option value="No Insurance Cash Pay">No Insurance Cash Pay</option>
                        <option value="No Insurance, Will Need Financial Assistance">No Insurance, Will Need Financial Assistance</option>
                    </select>
                </div><!-- end type of insurance -->
            </div><!--end row for insurance type-->

        </div><!-- end inner -->
        <div class="alt inner">
            <h5 class="clock-icon">Want to speed up the process? <span class="optional">- Optional</span></h5>
            <p>If you can provide a little more information now, we'll go ahead and get to work on finding personalized options based on your situation and coverage.</p>

            <div id="speed-up-fields">
                <input type="radio" name="speed-up" id="speed-up-yes" checked=""><label for="speed-up-yes">Yes</label>
                <input type="radio" name="speed-up" id="speed-up-no"><label for="speed-up-no">No thanks</label>
                <div id="additional-fields">
                    <div class="row grid-margin-x">
                        <div class="large-12 columns">
                            <label for="00N0a00000CVU7F">
                                <h5><span class="patient-reveal">Patient</span> Insurance Company <span class="optional">- Optional</span><span data-tooltip="" tabindex="1" title="" data-tooltip-class="tooltip insurance-company" aria-describedby="hq90oi-tooltip" data-yeti-box="hq90oi-tooltip" data-toggle="hq90oi-tooltip" data-resize="hq90oi-tooltip" class="has-tip" data-e="2joh75-e" data-events="resize"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/vob-images/question-circle.png" width="15" height="15" class="form-tool-tip"></span></h5>
                            </label>
                            <input id="00N0a00000CVU7F" maxlength="255" name="00N0a00000CVU7F" type="text">
                        </div><!-- end columns for the insurance company name-->

                        <div class="large-12 columns">
                            <label for="00N0a00000CVU7G">
                                <h5><span class="patient-reveal">Patient</span> Insurance ID Number <span class="optional">- Optional</span><span data-tooltip="" tabindex="1" title="" data-tooltip-class="tooltip insurance-id" aria-describedby="72hfig-tooltip" data-yeti-box="72hfig-tooltip" data-toggle="72hfig-tooltip" data-resize="72hfig-tooltip" class="has-tip" data-e="flj1ij-e" data-events="resize"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/vob-images/question-circle.png" width="15" height="15" class="form-tool-tip"></span> </h5>
                            </label>
                            <input id="00N0a00000CVU7G" maxlength="50" name="00N0a00000CVU7G" type="text">
                        </div><!-- end columns for the insurance ID number-->

                        <div class="large-12 columns">
                            <label for="00N0a00000CVU7H">
                                <h5><span class="patient-reveal">Patient</span> Insurance Phone Number <span class="optional">- Optional</span><span data-tooltip="" tabindex="1" title="" data-tooltip-class="tooltip insurance-phone" aria-describedby="jttvqg-tooltip" data-yeti-box="jttvqg-tooltip" data-toggle="jttvqg-tooltip" data-resize="jttvqg-tooltip" class="has-tip" data-e="c5oclj-e" data-events="resize"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/vob-images/question-circle.png" width="15" height="15" class="form-tool-tip"></span></h5>
                            </label>
                            <input id="00N0a00000CVU7H" maxlength="50" name="00N0a00000CVU7H" type="text">
                        </div><!-- end large-12-->

                        <div class="large-6 medium-6 columns end">
                            <label for="00Nj000000BKJjV"><h5><span class="patient-reveal">Patient</span> Date of Birth <span class="optional">- Optional</span></h5></label><span class="dateInput dateOnlyInput"><input id="00Nj000000BKJjV" name="00Nj000000BKJjV" type="text" placeholder="MM/DD/YYYY"></span>
                        </div><!-- end columns for the DOB-->

                    </div><!-- end grid row --> 
                    <label for="00Nj000000BKJk6">
                        <h5>Tell us about your situation: <span class="optional">- Optional</span></h5>
                    </label>
                    <textarea placeholder="Tell us a little bit about what is going on..." cols="30" rows="6" id="00Nj000000BKJk6" name="00Nj000000BKJk6" type="text"></textarea>
                </div><!-- end #additional fields -->
            </div><!-- end speed up feilds -->
        </div><!-- end inner alt -->
        <div id="hidden-info">
            <select id="lead_source" name="lead_source" class="hide">
                <option value="Online" selected="">Online</option>
            </select>

            <input id="00N0a00000CRYdN" maxlength="150" name="00N0a00000CRYdN" size="20" type="text" value="Web MHouse" class="hide">

            <textarea id="00Nj000000BKJjr" name="00Nj000000BKJjr" rows="1" type="text" value="https://michaelshouse.com" class="hide">https://michaelshouse.com</textarea>

            <select id="00Nj000000BKJjt" name="00Nj000000BKJjt" title="Origin" class="hide">
                <option value="Web Form" selected="">Web Form</option>
            </select>

            <select id="country_code" name="country_code" class="hide"><option value="US">US</option></select>
        </div><!-- end hidden info -->

        <div class="inner">
            <div class="g-recaptcha" data-sitekey="6LehmHoUAAAAAAxLWRnOW1P3c2EftdBERtQ2XBEM" data-callback="recaptcha_callback"></div>
            <div class="row grid-margin-x">
                <div class="large-6 medium-6 columns">
                    <input type="submit" class="button button--secure" value="Submit Form" disabled="true" onclick="ga('send', 'event', 'VOB Form', 'Submit Form', 'VOB Form Launch');">
                </div><!-- end columns with button -->
            </div><!-- end row for button and logo -->
        </div><!-- end inner -->
    </form><!-- end the form -->
</div><!-- end #vob-holder -->