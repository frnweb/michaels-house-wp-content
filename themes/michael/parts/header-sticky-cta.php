<div id="sl_sticky" class="sl_sticky sl_sticky--contact show-for-medium" data-toggler=".sl_active">
    <div class="sl_sticky__open" data-toggle="sl_sticky">
        Questions? Get help now.
    </div>
    <div class="sl_card">
        <h3>We know this is hard, you are not alone.</h3>
        <?php echo do_shortcode('[lhn_inpage button="chat" text="Chat with a specialist" offline_text="Email" class="sl_sticky_btn"]'); ?>
        <div class="sl_phone">
            <span class="sl_phone__cta">Or call us, it's free and private</span>
            <?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Module" class="sl_sticky_btn--phone sl_phone__number"]'); ?>
        </div>
        <a class="sl_close_btn" data-toggle="sl_sticky">No thanks</a>
    </div>
</div>