<?php
/**
* Template Name: Home Page
*/
get_header();
?>
  <!-- Main Content -->
    <div id="sl_homepage" class="sl_main_content">

        <!-- Billboard -->
        <section class="sl_module sl_module--billboard sl_billboard">
            <div class="sl_inner">
                <div class="sl_billboard__content">
                    <h1>An Oasis of Compassion, Hope, and Restoration</h1>
                    <p>Celebrating 30 years of trusted, evidence-based treatment</p>
                        <div class="sl_button-group">
                            <a href="/treatment-program/" class="sl_button sl_button--primary">Get Help Now</a>
                            <?php echo do_shortcode('[frn_phone class="sl_button sl_button--border" ga_phone_location="Phone Clicks in Admissions Coordinator" ]'); ?>
                        </div><!-- /.sl_button-group -->
                </div><!-- /.sl_billboard__content -->
            </div><!-- /.sl_inner-->
        </section>
        <!-- End Billboard -->

        <!-- Basic -->
        <section class="sl_module sl_module--basic sl_basic sl_basic--inverse">
            <div class="sl_inner">
                    <h2 style="text-align: center;">A lifetime of recovery is just one phone call away.</h2>
                    <p style="text-align: center;">Take a step towards a happier, healthier life.</p>
                    <div class="sl_row">
                        <div class="sl_cell large-3 medium-12 small-12">
                        </div>
                        <div class="sl_cell large-6 medium-12 small-12">
                            <div class="flex-video widescreen">
                                <iframe width="100%" height="100%" src="https://www.youtube.com/embed/aIKxULuzvw8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="sl_cell large-3 medium-12 small-12">
                        </div>
                </div><!-- /.sl_billboard__content -->
            </div><!-- /.sl_inner-->
        </section>
        <!-- End Basic -->

         <!-- About Michael -->
        <section class="sl_module sl_module--duo sl_duo">
            <div class="sl_inner sl_inner--expanded">
                <div class="sl_row">
                    <div class="sl_cell sl_duo__content large-6 medium-12 small-12">
                        <div class="sl_card sl_duo__card">
                            <h2>A History of Hope Restored</h2>
                            <p>Michael’s House is dedicated to providing comprehensive, evidence-based, integrated treatment that addresses both substance use and co-occurring mental health issues. For over 30 years, our evidence-based methods have resulted in healthy rates of long-term recovery, and our commitment to patient-centered care affords us the flexibility to address each patient’s unique needs.</p>
                            <a class="sl_button sl_button--simple" href="/about/">Learn more about us</a>
                            <div class="sl_duo__accreditations sl_row">
                                <div class="sl_cell auto sl_accred">
                                    <a href="http://www.carf.org/providerProfile.aspx?cid=246451" target="_blank">
                                        <img alt="CARF Accredited" src="/wp-content/themes/michael/images/accreditations/CARF_logo.png">
                                    </a>
                                </div><!--/.sl_cell-->
                                <div class="sl_cell auto sl_accred">
                                    <a href="https://www.naatp.org/resources/addiction-industry-directory/1382/michaels-house-treatment-center" target="_blank">
                                        <img alt="NAATP Provider Member" src="https://www.naatp.org//civicrm/file?reset=1&id=4087&eid=201&fcs=1f26e81149c63e3ea4639b16e1ccf9e6a66df075bd944bb7ddb8b7024507c576_1585039261_87600">
                                    </a>
                                </div><!--/.sl_cell-->
                                <div class="sl_cell auto sl_accred">
                                    <script src="https://static.legitscript.com/seals/786341.js"></script>
                                </div><!--/.sl_cell-->
                            </div><!--/.sl_duo__accrediations-->
                        </div><!--/.sl_duo__card-->
                    </div><!--/.sl_duo__content-->
                    <div class="sl_cell sl_duo__media large-6 medium-12 small-12">
                        <div class="sl_duo__slider">
                            <div><img src="wp-content/themes/michael/images/assets/gallery/MH-RTC-Building.jpg"></div>
                            <div><img src="wp-content/themes/michael/images/assets/gallery/MH-RTC-Cafeteria.jpg"></div>
                            <div><img src="wp-content/themes/michael/images/assets/gallery/MH-RTC-Pool.jpg"></div>
                            <div><img src="wp-content/themes/michael/images/assets/gallery/MH-TL-Arbor.jpg"></div>
                            <div><img src="wp-content/themes/michael/images/assets/gallery/MH-TL-Arial.jpg"></div>
                            <div><img src="wp-content/themes/michael/images/assets/gallery/MH-TL-Courtyard.jpg"></div>
                            <div><img src="wp-content/themes/michael/images/assets/gallery/MH-TL-TreeCourtyard.jpg"></div>
                        </div><!--/.sl_duo__slider-->
                    </div><!--/.sl_duo__media-->
                </div><!--/.sl_row-->  
            </div><!--/.sl_inner-->
        </section>
        <!-- About Michael Ends -->

        <!-- Levels of Care Duo Accordion -->
        <section class="sl_module sl_module--duo sl_duo sl_duo--dropdown">
            <div class="sl_inner">
                <div class="sl_row">
                    <div class="sl_cell sl_duo__content medium-6 small-12">
                        <div class="sl_card sl_dropdown_duo__card">
                            <h2>Ready to Meet You Where You Are</h2>
                            <p>Our accredited treatment programs at Michael’s House are nationally recognized for our integrated and evidence-based methods that guide patients with addiction and mental health disorders toward long-term recovery. Boasting a full continuum of care, Michael’s House is prepared to meet the unique needs of each individual patient.</p>
                        </div><!--/.sl_card-->
                    </div><!--/.sl_duo--dropdown__content-->
                    <div class="sl_cell sl_duo__dropdown medium-6 small-12">
                        <ul class="accordion sl_accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">
                            <li class="accordion-item sl_accordion__item" data-accordion-item>
                                <a href="#" class="accordion-title sl_accordion__title">Admission/Intake</a>
                                <div class="accordion-content sl_accordion__content" data-tab-content>
                                <p>Admissions begin with a free, confidential assessment conducted by an experienced admissions coordinator. Our staff will then help you determine the most appropriate level of care for yourself or your loved one. We’ll guide you through the process step-by-step so you can be confident you’re taking all the right steps</p>
                                <a class="sl_button sl_button--simple" href="/treatment-admissions/">Our Admissions Process</a>                                
                                </div><!--/.sl_accordion__content-->
                            </li>
                            <li class="accordion-item sl_accordion__item" data-accordion-item>
                                <a href="#" class="accordion-title sl_accordion__title">Detox and Stabilization</a>
                                <div class="accordion-content sl_accordion__content" data-tab-content>
                                <p>Detox is typically the first step in treatment and usually lasts two to four days. During detox, patients are medically monitored 24 hours a day with a focus on safety and comfort to ensure you are physically and mentally stabilized and ready to participate in the residential treatment program.</p>
                                </div><!--/.sl_accordion__content-->
                            </li>
                            <li class="accordion-item sl_accordion__item" data-accordion-item>
                                <a href="#" class="accordion-title sl_accordion__title">Residential Treatment</a>
                                <div class="accordion-content sl_accordion__content" data-tab-content>
                                <p>Residential treatment allows patients to dedicate all of their time and energy toward their healing and growth. By focusing on yourself, you improve your own life while preparing to return to everyday life as a better parent, child, partner, or friend.</p>
                                <a class="sl_button sl_button--simple" href="/locations/michaels-house-treatment-center/">Learn more about Residential Treatment</a>  
                                </div><!--/.sl_accordion__content-->
                            </li>
                            <li class="accordion-item sl_accordion__item" data-accordion-item>
                                <a href="#" class="accordion-title sl_accordion__title">Outpatient Treatment</a>
                                <div class="accordion-content sl_accordion__content" data-tab-content>
                                <p>For those who need a less-structured setting for treatment, or are ready to step down from residential or PHP treatment, our outpatient program is dedicated to providing continued support and helping patients improve their overall quality of life in recovery.</p>
                                <a class="sl_button sl_button--simple" href="/locations/outpatient-program/">Learn more about Outpatient Treatment</a>
                                </div><!--/.sl_accordion__content-->
                            </li>
                        </ul>
                    </div><!--/.sl_duo--dropdown__dropdown-->
                </div><!--/.sl_row-->
            </div><!--/.sl_inner-->         
        </section>
        <!-- End Levels of Care Duo Accordion -->

        <!-- Progam Tabs -->
        <?php //get_template_part( 'parts/module', 'programs' ); ?>
        <!-- End Program Tabs -->

        <!-- Testimonial Carousel -->
        <section class="sl_module sl_module--testimonial sl_testimonial">
            <div class="sl_inner">
                <div class="sl_row">
                    <div class="sl_cell sl_testimonial__carousel medium-6 small-12">
                        <div class="sl_testimonial__slider">
                            <!-- First Testimonial -->
                            <div>
                                <blockquote>
                                    <p>They aren’t just telling you how to get better. They are showing you and giving you life lessons. They gave me my spirit back.</p>
                                    <cite>
                                        <span class="sl_cite__name">Dawn F.</span>
                                        <span class="sl_cite__type">Michael's House Alumni</span>
                                    </cite>
                                </blockquote>
                            </div>
                            <!-- End First Testimonial -->

                            <!-- Second Testimonial -->
                            <div>
                                <blockquote>
                                    <p>It really is the source of everything good and everything great that my life is today – because it all started there.</p>
                                    <cite>
                                        <span class="sl_cite__name">Christina</span>
                                        <span class="sl_cite__type">Michael's House Alumni</span>
                                    </cite>
                                </blockquote>
                            </div>
                            <!-- End Second Testimonial -->

                            <!-- Third Testimonial -->
                            <div>
                                <blockquote>
                                    <p>Michael’s House saved my loved ones and saved my life and saved my family’s lives. Plain and simple.</p>
                                    <cite>
                                        <span class="sl_cite__name">Matt and Tim</span>
                                        <span class="sl_cite__type">Michael's House Alumni</span>
                                    </cite>
                                </blockquote>
                            </div>
                            <!-- End Third Testimonial -->
                        </div><!-- sl_testimonial__slider -->
                    </div><!-- sl_testimonial__carousel -->
                    <div class="sl_cell sl_testimonial__media medium-6 small-12">
                        <div class="sl_testimonial__media__container">
                            <div class="sl_testimonial__image" style="background-image:url('/wp-content/themes/michael/images/assets/MH-Treatment-Entrance.jpg')">
                                    <a class="sl_testimonial__button" id="homepage_video_pop" href="#homepage_video_pop" data-open="video" onClick="frn_reporting('','Content Interactions', 'Banner Video Popup', 'Homepage',false,'');"><img src="/wp-content/themes/michael/images/assets/icons/play-video.png"></a>
                            </div><!-- sl_testimonial__image -->
                        </div><!-- sl_testimonial__media__container -->
                    </div><!-- sl_testimonial__media -->
                </div><!-- sl_row -->
            </div><!-- sl_inner -->
        </section>
        <!-- End Testomonial Carousel -->

        <!-- Testimonial Carousel -->
        <section class="sl_module sl_module--testimonial-duo sl_testimonial-duo">
            <div class="sl_inner">
                <div class="sl_testimonial-duo__slider">

                    <!-- First Testimonial -->
                    <div>
                        <div class="sl_row">
                            <div class="sl_cell sl_testimonial-duo__media medium-6 small-12">
                                <div class="sl_testimonial-duo__media__container">
                                    <div class="sl_testimonial-duo__image" style="background-image:url('/wp-content/themes/michael/images/assets/testimonial/testimonial-5.jpg')">
                                    </div><!-- sl_slide__image -->
                                </div><!-- sl_slide__media__container -->
                            </div><!-- sl_slide__media -->

                            <div class="sl_cell sl_testimonial-duo__content medium-6 small-12">
                                <blockquote>
                                    <p>It is comfortable here in a way that isn’t comfortable in other places</p>
                                    <cite>
                                        <span class="sl_cite__name">Anonymous</span>
                                        <span class="sl_cite__type">Michael's House Alumni</span>
                                    </cite>
                                </blockquote>
                            </div><!-- sl_testimonial__carousel -->
                        </div><!-- sl_row -->
                    </div>
                    <!-- End First Testimonial -->

                    <!-- Second Testimonial -->
                    <div>
                        <div class="sl_row">
                            <div class="sl_cell sl_testimonial-duo__media medium-6 small-12">
                                <div class="sl_testimonial-duo__media__container">
                                    <div class="sl_testimonial-duo__image" style="background-image:url('/wp-content/themes/michael/images/assets/testimonial/testimonial-2.jpg')">
                                    </div><!-- sl_slide__image -->
                                </div><!-- sl_slide__media__container -->
                            </div><!-- sl_slide__media -->

                            <div class="sl_cell sl_testimonial-duo__content medium-6 small-12">
                                <blockquote>
                                    <p>What I like the best about Michael’s House was how comforting & supportive they have been</p>
                                    <cite>
                                        <span class="sl_cite__name">Anonymous</span>
                                        <span class="sl_cite__type">Michael's House Alumni</span>
                                    </cite>
                                </blockquote>
                            </div><!-- sl_testimonial__carousel -->
                        </div><!-- sl_row -->
                    </div>
                    <!-- End Second Testimonial -->

                    <!-- Third Testimonial -->
                    <div>
                        <div class="sl_row">
                            <div class="sl_cell sl_testimonial-duo__media medium-6 small-12">
                                <div class="sl_testimonial-duo__media__container">
                                    <div class="sl_testimonial-duo__image" style="background-image:url('/wp-content/themes/michael/images/assets/testimonial/testimonial-3.jpg')">
                                    </div><!-- sl_slide__image -->
                                </div><!-- sl_slide__media__container -->
                            </div><!-- sl_slide__media -->
                            
                            <div class="sl_cell sl_testimonial-duo__content medium-6 small-12">
                                <blockquote>
                                    <p>Like the variety of sessions - especially that they aren’t all AA</p>
                                    <cite>
                                        <span class="sl_cite__name">Anonymous</span>
                                        <span class="sl_cite__type">Michael's House Alumni</span>
                                    </cite>
                                </blockquote>
                            </div><!-- sl_testimonial__carousel -->
                        </div><!-- sl_row -->
                    </div>
                    <!-- End Third Testimonial -->

                    <!-- Fourth Testimonial -->
                    <div>
                        <div class="sl_row">
                            <div class="sl_cell sl_testimonial-duo__media medium-6 small-12">
                                <div class="sl_testimonial-duo__media__container">
                                    <div class="sl_testimonial-duo__image" style="background-image:url('/wp-content/themes/michael/images/assets/testimonial/testimonial-4.jpg')">
                                    </div><!-- sl_slide__image -->
                                </div><!-- sl_slide__media__container -->
                            </div><!-- sl_slide__media -->
                            
                            <div class="sl_cell sl_testimonial-duo__content medium-6 small-12">
                                <blockquote>
                                    <p>I like the fact everyone is expected to participate</p>
                                    <cite>
                                        <span class="sl_cite__name">Anonymous</span>
                                        <span class="sl_cite__type">Michael's House Alumni</span>
                                    </cite>
                                </blockquote>
                            </div><!-- sl_testimonial__carousel -->
                        </div><!-- sl_row -->
                    </div>
                    <!-- End Fourth Testimonial -->

                    <!-- Fifth Testimonial -->
                    <div>
                        <div class="sl_row">
                            <div class="sl_cell sl_testimonial-duo__media medium-6 small-12">
                                <div class="sl_testimonial-duo__media__container">
                                    <div class="sl_testimonial-duo__image" style="background-image:url('/wp-content/themes/michael/images/assets/testimonial/testimonial-1.jpg')">
                                    </div><!-- sl_slide__image -->
                                </div><!-- sl_slide__media__container -->
                            </div><!-- sl_slide__media -->
                            
                            <div class="sl_cell sl_testimonial-duo__content medium-6 small-12">
                                <blockquote>
                                    <p>I enjoy the classes and have learned a lot or refreshed some of what I had already learned</p>
                                    <cite>
                                        <span class="sl_cite__name">Anonymous</span>
                                        <span class="sl_cite__type">Michael's House Alumni</span>
                                    </cite>
                                </blockquote>
                            </div><!-- sl_testimonial__carousel -->
                        </div><!-- sl_row -->
                    </div>
                    <!-- End Fifth Testimonial -->

                </div><!-- sl_testimonial-duo__slider -->
            </div><!-- sl_inner -->
        </section>
        <!-- End Testomonial Carousel Duo -->

        <!-- CTA Spotlight -->
        <section class="sl_module sl_module--spotlight sl_spotlight">
            <div class="sl_inner">
            <div class="sl_row">
                <div class="sl_cell sl_spotlight__content medium-8 small-12">
                    <div class="sl_card sl_spotlight__card">
                        <p>We offer you accredited, <span style="white-space: nowrap">evidence-based</span> treatment options you can trust!</p>
                    </div><!-- sl_card -->
                </div><!-- sl_spotlight__content -->
                <div class="sl_cell sl_spotlight__buttons medium-4 small-12">
                    <?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Home Callout" class="sl_button sl_button--primary"]')?>
                    <a id="chat" class="sl_button sl_button--border" href="#" onclick="frn_open_lhnwindow('chat','Global Contact Options'); return false;">Chat With Us</a>
                </div><!-- sl_spotlight__buttons -->
            </div><!-- sl_row -->
            </div><!-- sl_inner -->
        </section>
        <!-- End CTA Spotlight -->

        <!-- Insurance Module -->
        <section class="sl_insurance">
            <div class="sl_inner">
                <div class="sl_row">
                    <div class="sl_cell sl_insurance__content medium-6 small-12">
                        <div class="sl_card sl_insurance__card">
                            <h2>Treatment Admissions</h2>
                            <p>Michael’s House works with many insurance providers to guarantee you the highest quality of treatment at the most affordable cost. Our admissions coordinators are always on your side and will make the process as simple and straightforward as possible.</p>
                            <a class="sl_button sl_button--simple" href="/treatment-admissions/">Learn more about the admissions process</a>
                            <a class="sl_button sl_button--simple" href="/insurance/">Learn more about insurance coverage </a>
                        </div><!-- sl_card -->
                    </div><!-- sl_insurance__content -->
                    <div class="sl_cell sl_insurance__media medium-6 small-12">
                        <div class="sl_row sl_insurance__grid large-up-3 medium-up-3 small-up-3">
                            <div class="sl_cell sl_insurance__logo">
                                <img alt="4 Your Choice Insurance" src="/wp-content/themes/michael/images/assets/insurance/4yourchoice.png">
                            </div><!-- sl_insurance__logo -->
                            <div class="sl_cell sl_insurance__logo">
                                <img alt="Blue Cross Blue Shield Insurance" src="/wp-content/themes/michael/images/assets/insurance/bluecrossblueshield.png">
                            </div><!-- sl_insurance__logo -->
                            <div class="sl_cell sl_insurance__logo">
                                <img alt="ComPsych Insurance" src="/wp-content/themes/michael/images/assets/insurance/compsych.png">
                            </div><!-- sl_insurance__logo -->
                            <div class="sl_cell sl_insurance__logo">
                                <img alt="Aetna Insurance" src="/wp-content/themes/michael/images/assets/insurance/aetna.png">
                            </div><!-- sl_insurance__logo -->
                            <div class="sl_cell sl_insurance__logo">
                                <img alt="Humana Insurance" src="/wp-content/themes/michael/images/assets/insurance/humana.png">
                            </div><!-- sl_insurance__logo -->
                            <div class="sl_cell sl_insurance__logo">
                                <img alt="Valueoptions insurance" src="/wp-content/themes/michael/images/assets/insurance/valueoptions.png">
                            </div><!-- sl_insurance__logo -->
                            <div class="sl_cell sl_insurance__logo">
                                <img alt="Cigna insurance" src="/wp-content/themes/michael/images/assets/insurance/cigna.png">
                            </div><!-- sl_insurance__logo -->
                            <div class="sl_cell sl_insurance__logo">
                                <img alt="Anthem insurance" src="/wp-content/themes/michael/images/insurance-logos/anthem.png">
                            </div><!-- sl_insurance__logo -->
                            <div class="sl_cell sl_insurance__logo">
                                <img alt="Tricare insurance" src="/wp-content/themes/michael/images/assets/insurance/tricare.png">
                            </div><!-- sl_insurance__logo -->
                            <div class="sl_cell sl_insurance__logo">
                                <img alt="Magellan insurance" src="/wp-content/themes/michael/images/assets/insurance/magellan.png">
                            </div><!-- sl_insurance__logo -->
                            <div class="sl_cell sl_insurance__logo">
                                <img alt="Prime Healthcare insurance" src="/wp-content/themes/michael/images/insurance-logos/prime.png">
                            </div><!-- sl_insurance__logo -->
                            <div class="sl_cell sl_insurance__logo">
                                <img alt="Beacon insurance" src="/wp-content/themes/michael/images/assets/insurance/beaconhealth.png">
                            </div><!-- sl_insurance__logo -->
                        </div><!-- sl_insurance__grid -->
                    </div><!-- sl_insurance__media -->
                </div><!-- sl_row -->
            </div><!-- sl_inner -->
        </section>
        <!-- End Insurance Module -->

        <!-- Location Module -->
        <section class="sl_module sl_module--location sl_location">
            <div class="sl_inner">
                <div class="sl_row">
                    <div class="sl_cell sl_location__content medium-6 small-12" style="background-image:url(/wp-content/themes/michael/images/assets/MH-TC-Entrance.jpg)">
                        <div class="sl_card sl_location__card" >
                            <h2>Michael's House</h2>
                            <p>1910 S Camino Real<br>
                            Palm Springs, CA 92262</p>
                            <a class="sl_button sl_button--secondary" href="/contact-us/">Contact Us</a>
                        </div><!-- sl_card -->
                    </div><!-- sl_location__content -->
                    <div class="sl_cell sl_location__map medium-6 small-12">
                        <div class="sl_location__map__container">

                            <!-- Residential -->
                            <div class="marker" data-lat="33.7975004" data-lng="-116.5385655">
                                <div class="sl_card sl_card--map">
                                    <div class="sl_row">
                                        <div class="sl_cell sl_card__image" style="background-image:url('/wp-content/themes/michael/images/assets/MH-Entrance.jpg')">
                                        </div><!--sl_card__image -->
                                        <div class="sl_cell sl_card__content">
                                            <h5>Palm Springs, CA</h5>
                                            <h3>Michael's House</h3>
                                            <a href="https://goo.gl/maps/Mvv52XKPNPrDsmddA" class="sl_button sl_button--simple">Get Directions</a>
                                        </div><!--sl_card__content -->
                                    </div><!-- sl_row -->
                                </div><!-- sl_card -->
                            </div><!-- marker -->

                            <!-- Outpatient -->
                            <div class="marker" data-lat="33.830574" data-lng="-116.5492154">
                                <div class="sl_card sl_card--map">
                                    <div class="sl_row">
                                        <div class="sl_cell sl_card__image" style="background-image:url('/wp-content/themes/michael/images/megamenu/MH-Outpatient.jpg')">
                                        </div><!--sl_card__image -->
                                        <div class="sl_cell sl_card__content">
                                            <h5>Palm Springs, CA</h5>
                                            <h3>Michael's House Outpatient</h3>
                                            <a href="https://goo.gl/maps/pR7Ggrzez6kmrPG8A" class="sl_button sl_button--simple">Get Directions</a>
                                        </div><!--sl_card__content -->
                                    </div><!-- sl_row -->
                                </div><!-- sl_card -->
                            </div><!-- marker -->

                            <!-- Transitional Living -->
                            <div class="marker" data-lat="33.816993" data-lng="-116.5505538">
                                <div class="sl_card sl_card--map">
                                    <div class="sl_row">
                                        <div class="sl_cell sl_card__image" style="background-image:url('/wp-content/themes/michael/images/megamenu/MH-Outdoor-Arial.jpg)')">
                                        </div><!--sl_card__image -->
                                        <div class="sl_cell sl_card__content">
                                            <h5>Palm Springs, CA</h5>
                                            <h3>Michael's House Transitional Living</h3>
                                            <a href="https://goo.gl/maps/eDqvivuia11G223w6" class="sl_button sl_button--simple">Get Directions</a>
                                        </div><!--sl_card__content -->
                                    </div><!-- sl_row -->
                                </div><!-- sl_card -->
                            </div><!-- marker -->

                        </div><!-- sl_location__map__container -->
                    </div><!-- sl_location__map -->             
                </div><!-- sl_row -->
            </div><!-- sl_inner -->
        </section>
        <!-- End Location Module -->
            
    </div><!-- sl_main_content -->

    <!-- Popup Video -->
    <div class="reveal sl_reveal sl_reveal--video" id="video" data-reset-on-close="true" data-reveal data-close-on-click="true">
        <div class="flex-video widescreen">
            <?php echo ot_get_option("iframe_code"); ?>
        </div>
        <button class="close-button" data-close aria-label="Close modal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>

<?php include 'footer.php';?>
