jQuery(document).ready(function( $ ){
    $('.sl_duo__slider').slick({
        // normal options...
        prevArrow: `<button class="sl_button--carousel sl_prev"></button>`,
        nextArrow: `<button class="sl_button--carousel sl_next"></button>`,
        dots:true,
        swipeToSlide: true,
        infinite: true,
        // the magic
        responsive: [{

            breakpoint: 1024,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            }

        }, {

            breakpoint: 767,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                swipeToSlide: true,
                arrows: false,
            }

        }]
    });

    $('.sl_testimonial__slider').slick({
        // normal options...
        prevArrow: `<button class="sl_button--carousel sl_prev"></button>`,
        nextArrow: `<button class="sl_button--carousel sl_next"></button>`,
        dots:true,
        swipeToSlide: true,
        infinite: true,

        // the magic
        responsive: [{

            breakpoint: 1024,
            settings: {
                slidesToShow: 1,
                arrows: false,
            }

        }, {

            breakpoint: 640,
            settings: {
                slidesToShow: 1,
                arrows: false,
                touchMove: true,
            }

        }]
    });

    $('.sl_testimonial-duo__slider').slick({
        // normal options...
        prevArrow: `<button class="sl_button--carousel sl_prev"></button>`,
        nextArrow: `<button class="sl_button--carousel sl_next"></button>`,
        dots:true,
        swipeToSlide: true,
        infinite: true,

        // the magic
        responsive: [{

            breakpoint: 1024,
            settings: {
                slidesToShow: 1,
                arrows: false,
            }

        }, {

            breakpoint: 640,
            settings: {
                slidesToShow: 1,
                arrows: false,
                touchMove: true,
            }

        }]
    });

    $('.sl_program__slider').slick({
        // normal options...
        arrows: false,
        dots:true,
        swipeToSlide: true,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1,
        infinite: true,
    });
});