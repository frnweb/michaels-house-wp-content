// import 'babel-polyfill'
import $ from 'jquery'
import 'slick-carousel/slick/slick'

import Foundation from 'foundation-sites'

$(document).foundation();

import slickCarousel from './lib/slick-carousel'
import googleMaps from './lib/google-maps'

if(window.navigator.userAgent.indexOf('MSIE ') > 0) {
    // IE 10 or older => return version number
    $('<div class="sl_ie-error"><p>You are using a browser that is not supported by the Google Maps JavaScript API. Consider changing your browser.</p><a href="https://browsehappy.com/">Upgrade your browser.</a></div>').insertBefore(".sl_location__map__container");
}