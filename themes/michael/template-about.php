<?php

/**

* Template Name: About 

*/

get_header();

?>
<?php while ( have_posts() ) : the_post();
$feature_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
if( ! $feature_image ) {
    $feature_image = ot_get_option("default_header_image");
}
?>
  <div class="main-content">
        <!-- Innerpage Banner -->
        
        <section class="banner banner-innerpage " style="background-image:url(<?php echo $feature_image;?>);">
            <!-- Innerpage Banner Caption -->
            <div class="caption">
                <h1><?php the_title();?></h1>
            </div>
        </section>
   
        <!-- Banner Ends -->
        <!-- Content Section -->
        <!-- Our Staff Detail -->
        <div class="row sec ">
            <div class="small-10 small-centered large-centered columns">
                <!-- Text Row -->
                <small><?php the_title();?></small>
                <?php the_content();?>
            </div>
        </div>
    <?php endwhile;?>
    <?php 
        $terms=get_field('choose_our_staff',$post->ID);
        $a = 0; foreach($terms as $t) : $a++;
            $term = $t['category'];
    ?>
        <!--Light Grey Background Section -->
        <section class="staff-sec text-center<?php if( $a === 1 ) echo ' bg_grey-lt' ?>">
        <?php  
               $term=get_term_by('id', $term->term_id,$term->taxonomy);
         ?>
            <div class="row">
                <h3><?php echo $term->name?></h3>
                <ul class="staff-list">
                <?php 
                query_posts(array(
                    'post_type'=>'staff',
                    'posts_per_page'=>-1, 
                    'tax_query' => array(
                        array(
                            'taxonomy' => $term->taxonomy,
                            'field' => 'id',
                            'terms' => $term->term_id 
                        )
                    )
                ));
                if ( have_posts() ) : while ( have_posts() ) : the_post();
                $feature_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );  ?>
                    <li>
                    <?php if($feature_image){?>
                        <figure>
                            <a href="<?php the_permalink();?>"><img src="<?php echo  $feature_image; ?>" alt="<?php the_title();?>"></a>
                        </figure>
                        <?php }?>
                        <a href="<?php the_permalink();?>"><?php the_title();?></a>
                        <p><?php the_field('designation');?></p>
                    </li>
                    <?php endwhile; endif; wp_reset_postdata(); wp_reset_query(); ?>
                </ul>
            </div>
        </section>
    <?php endforeach ?>
        

<?php get_footer();?>