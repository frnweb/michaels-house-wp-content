<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>

<!-- Footer -->
<footer id="sl_footer" class="sl_footer">

    <!-- Footer Callout -->
    <section class="sl_footer__callout">
        <div class="sl_inner">
            <h2>Give us a call <?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Admissions Coordinator" ]'); ?></h2>
            <div class="sl_button-group">
                <a id="chat" class="sl_button sl_button--primary" href="#" onclick="frn_open_lhnwindow('chat','Global Contact Options'); return false;">Chat With Us</a>
                <?php echo do_shortcode('[lhn_inpage button="email" class="sl_button sl_button--border" text="Email Us"]'); ?>
            </div><!-- sl_button-group -->
        </div><!-- sl_inner -->
    </section>

    <!-- Mailchimp Email Form -->
    <section class="sl_footer__email">
        <div class="sl_inner">
            <?php echo do_shortcode('[email]'); ?>
        </div><!-- sl_inner -->
    </section>

    <div class="sl_footer__menu">
        <div class="sl_inner">

            <div class="sl_footer__info sl_row">

                <!-- FOOTER LOGO SOCIAL AND ADDRESS -->
                <div class="sl_cell small-12 medium-4 large-4">
                    <div class="sl_footer__brand">
                        <a class="sl_footer__brand__logo hide-for-small-only" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="michael's house"> <img src="<?php echo ot_get_option("header_logo"); ?>" alt="footer-logo"> </a>

                        <!-- Social Icons -->
                        <ul class="sl_footer__brand__social">
                            <li><a href="<?php echo ot_get_option("twitter_link"); ?>"<?=$target;?> ><i class="fa fa-twitter"></i></a></li>
                            <li><a href="<?php echo ot_get_option("facebook_link"); ?>"<?=$target;?> ><i class="fa fa-facebook"></i></a></li>
                            <li><a href="<?php echo ot_get_option("linkedin_link"); ?>"<?=$target;?> ><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="<?php echo ot_get_option("youtube"); ?>"<?=$target;?> ><i class="fa fa-youtube-play"></i></a></li>
                        </ul>
                    </div><!-- /.sl_footer__brand -->

                    <div class="sl_footer__address">
                        <a href="https://goo.gl/maps/kugLLa8f85joYS3D6" target="_blank"> <?php echo ot_get_option("address"); ?></a>
                    </div><!-- /.sl_footer__address -->
                </div><!-- /.sl_cell -->
                <!-- END FOOTER LOGO SOCIAL AND ADDRESS -->

                <!-- FOOTER ACCREDITATIONS -->
                <div class="sl_cell small-12 medium-5 large-5">
                    <div class="sl_footer__accred">

                        <!-- NAATP -->
                        <div class="sl_footer__accred__logo">
                            <a href="https://www.naatp.org/resources/addiction-industry-directory/1382/michaels-house-treatment-center" target="_blank">
                                <img alt="NAATP Provider Member" src="https://www.naatp.org//civicrm/file?reset=1&id=4087&eid=201&fcs=1f26e81149c63e3ea4639b16e1ccf9e6a66df075bd944bb7ddb8b7024507c576_1585039261_87600">
                            </a>
                        </div><!-- sl_footer__accred__logo -->

                        <!-- LEGITSCRIPT -->
                        <div class="sl_footer__accred__logo">
                            <script src="https://static.legitscript.com/seals/786341.js"></script>
                        </div><!-- sl_footer__accred__logo -->

                    </div><!-- /.sl_footer__accred -->
                </div><!-- /.sl_cell -->
                <!-- END FOOTER ACCREDITATIONS -->

                <!-- BILL PAY BUTTON -->
                <div class="sl_cell small-12 medium-3 large-3">
                    <a class="sl_button sl_button--billpay" href="/online-bill-pay/">Bill Pay</a>
                </div><!-- sl_cell -->
            </div><!-- sl_footer__info -->

            <!-- FOOTER Navigation -->
            <div class="sl_footer__navigation hide-for-small-only">
                <div class="sl_row">

                    <div class="sl_cell">
                        <p>Explore Our Site</p>
                        <?php wp_nav_menu(array('container' => '', 'menu' => 'Explore our site', 'menu_class' => '')); ?>
                    </div><!-- sl_cell -->

                    <div class="sl_cell">
                        <p>Research and Learn</p>
                        <?php wp_nav_menu(array('container' => '', 'menu' => 'Research & Learn', 'menu_class' => '')); ?>
                    </div>

                    <div class="sl_cell">
                        <p>Get in Touch</p>
                        <?php wp_nav_menu(array('container' => '', 'menu' => 'Get In Touch', 'menu_class' => '')); ?>
                    </div>
                 
                    <div class="sl_cell">
                        <p>Heal With Us</p>
                        <?php wp_nav_menu(array('container' => '', 'menu' => 'Heal With Us', 'menu_class' => '')); ?>
                    </div>

                    <div class="sl_cell">
                        <p>Meet Our Network</p>
                        <?php wp_nav_menu(array('container' => '', 'menu' => 'Meet Our network', 'menu_class' => '')); ?>
                    </div>
                </div><!-- /.sl_row -->
            </div><!-- /.sl_footer__menu -->

            <!-- Footer Bottom -->
            <div class="sl_footer__bottom">
                <p><?php echo ot_get_option("proud_member_text"); ?> </p>
                <div class="frn_footer">
                    <?php echo do_shortcode(ot_get_option("copyright")); ?>
                </div>
            </div>
        </div><!-- /.sl_inner -->
    </div><!-- sl_footer__menu -->
</footer> 
   	   
       <!-- Stylesheet -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/foundation.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/slick.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/app.css?ver=1.2">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style-responsive.css?v=1.03">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/frn.css?ver=2.3">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dist/css/main.css?ver=1.24">


    <!-- Javascript
    this is the old pyxl script
    <script src="https://use.typekit.net/lwz0pjg.js"></script>
    ben wrights old script <script src="https://use.typekit.net/mzm7wxg.js"></script>-->
    <script src="https://use.typekit.net/zrb6jyz.js"></script>
    

    <script>
    try {
        Typekit.load({
            async: true
        });
    } catch (e) {}
    </script>

    <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/what-input.js"></script>

    <script src="<?php echo get_template_directory_uri(); ?>/js/vob-conditional-form.js"></script><!-- VOB CONDITIONAL LOGIC -->
 	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.cookie.js"></script><!-- added this from old MH site for cookie -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/app.js?v=1.05" ></script>

<script>

jQuery(document).ready(function() {
	
	jQuery('.dropdown').each(function(){
		
		if(jQuery(this).find('.submenu_class').length==0)
		{
			        jQuery(this).children('ul').remove();
		}
		});
});
</script>
	<?php wp_footer(); ?>

</div><!-- Closes Offcanvas Content -->
</div><!-- Closes Offcanvas Wrapper -->
</body>
</html>