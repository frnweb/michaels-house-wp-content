jQuery('body').on('change.zf.tabs', function(e){
    jQuery(window).trigger('resize');
});

jQuery(document).ready(function() {
    //disables default jump to top of page function if # is in the href
    jQuery('a[href="#"]').click(function(e){
        e.preventDefault();
    });

    jQuery('.menu-toggle').click(function() {
        jQuery('header').toggleClass('uber-mobile');
    });
    jQuery('.menu-toggle').on('click', function() {
        frn_reporting('','Mobile Menu', 'Menu '+(jQuery(this).text().trim()=='MENU' ? "Open" : "Close"), false, '');
        //alert("Menu "+(jQuery(this).text().trim()=='MENU' ? "Open" : "Close"));
        jQuery('.menu-icon').html(jQuery('.menu-icon').html() == 'MENU' ? 'CLOSE' : 'MENU');
    });
    jQuery('.smoothscroll').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = jQuery(this.hash);
            target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                jQuery('html, body').animate({
                    scrollTop: target.offset().top - 79
                }, 1000);
                return false;
            }
        }
    });

    jQuery('.info-btn').click(function() {

        frn_reporting('','Global Contact Options', 'Main Contact Button', false, '');
        //alert("Contact button clicked");

        if (jQuery('.info__list .info__list__item').hasClass('active')) {
            jQuery('.info__list .info__list__item').removeClass('active');
            jQuery('.info__list .info__list__item').each(function() {
                jQuery(this).animate({ top: "0", opacity: "0", "z-index": "-1", "display": "none" }).delay(200);
            });
            jQuery('.info__list').css({ "display": "none" }).delay(200);
        } else {
            jQuery('.info__list .info__list__item').addClass('active');
            jQuery('.info__list .info__list__item').each(function() {
                jQuery(this).animate({ top: "-20px", opacity: "1", "z-index": "1", "display": "block" }).delay(200);
            });
            jQuery('.info__list').css({'display': 'block'}).delay(200);
        }
    });

    /*
    jQuery('#chat').click(function(e) {
        e.preventDefault();
        // ga('send', 'event', 'Global Contact Options', 'Chat/Email'); //removed since controlled by FRN plugin
    });
    
    jQuery('#mail').click(function(e) {
        e.preventDefault();
        // ga('send', 'event', 'Global Contact Options', 'Email via Contact Page'); //removed since controlled by FRN plugin
    });

    jQuery('#phone').click(function(e) {
        e.preventDefault();
        // ga('send', 'event', 'Global Contact Options', 'Custom Buttons: Phone Touches', 'Calls'); //removed since controlled by FRN plugin
    });
    */

    jQuery('#what_to_expect').click(function(e) {
        frn_reporting('','Content Interactions', 'What to Expect Popup', 'Header', false, '');
        // ga('send', 'event', 'Content Interactions', 'What to Expect Popup', 'Header');
    });

    jQuery('.orbit-next').click(function(e) {
        frn_reporting('','Content Interactions', 'Testimonial Slider', 'Next Testimonial', false, '');
        // ga('send', 'event', 'Content Interactions', 'Testimonial Slider', 'Next Testimonial');
    });

    jQuery('.orbit-previous').click(function(e) {
        frn_reporting('','Content Interactions', 'Testimonial Slider', 'Previous Testimonial', false, '');
        // ga('send', 'event', 'Content Interactions', 'Testimonial Slider', 'Previous Testimonial');
    });


    //Facility photo galleries
    jQuery('.amazingslider-arrow-left-3').click(function(e) {
        frn_reporting('','Content Interactions', 'Facility Photo Gallery', 'Previous Photo', false, '');
        // ga('send', 'event', 'Content Interactions', 'Testimonial Slider', 'Previous Testimonial');
    });
    jQuery('.amazingslider-arrow-right-3').click(function(e) {
        frn_reporting('','Content Interactions', 'Facility Photo Gallery', 'Next Photo', false, '');
        // ga('send', 'event', 'Content Interactions', 'Testimonial Slider', 'Previous Testimonial');
    });
    jQuery('.amazingslider-bullet-3').click(function(e) { //clicking a specific image
        frn_reporting('','Content Interactions', 'Facility Photo Gallery', jQuery(this).children("div").children("img").attr("src"), false, '');
        // ga('send', 'event', 'Content Interactions', 'Testimonial Slider', 'Previous Testimonial');
    });


    jQuery('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        adaptiveHeight: true,
        asNavFor: '.slider-nav'

    });
    jQuery('.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        arrows: false,
        centerMode: true,
        focusOnSelect: true
    });

    jQuery('.test-slider').slick({
        dots: false,
        arrows: false
    });
    jQuery('#homepage_video_pop').on('click', function() {
        frn_reporting('','Content Interactions', 'Banner Video Popup Open', 'Homepage', false, '');
        //alert("Video Popup Open Recorded");
    });
    jQuery('.reveal-overlay').on('click', function() {
        frn_reporting('','Content Interactions', 'Banner Video Popup Close', 'Homepage', false, '');
        //alert("Video Popup Close Recorded");
        // jQuery('#video').foundation('close');
    });

    // Dropdown
    // jQuery('.has-submenu').hover(function(){
    // jQuery('.dropdown-pane').removeClass('active');
    // jQuery('body').addClass('active');

    // });

    // function to trigger animation
    // document.querySelector('.info-btn').addEventListener('click', () => {
    //   document.querySelector('.info__list')
    //     .classList.toggle('info__list--animate');
    // });

    // Accordion Menu 
    /* jQuery('.menu-mobile').on('click', '.forced span', function() {
        window.location.href = jQuery(this).siblings('a').attr('href');
    });
    jQuery('.forced > a').html("");
    jQuery('.forced > span').text(jQuery('.forced > span').data("item"));*/
    // Duplicate first sub menu item for mobile so you can open the accordion as well as click on the menu item
    /*  jQuery('ul.menu-mobile.accordion-menu ul').each(function(){
    var parent = jQuery(this).parent();
    jQuery(this).prepend( "<li class='hide-for-medium'><a href='" + parent.find('a:first').attr('href') + "'>" + parent.find('a:first').html() + " Summary</a></li>" );
    });*/

    jQuery('ul.menu-mobile.accordion-menu li').on('click', function (){
    	jQuery(this).siblings('ul.menu-mobile.accordion-menu li ul').find('a').first().trigger('click');
        frn_reporting('','Content Interactions', 'Accordian Mobile Menu', false, '');
    });
    jQuery('.accordion-title').click(function(e) { //Mostly used on Resources page where categories are listed as blocks
        //upon click, the aria-expanded changes to true before the rest of this happens. So testing for true will mean the item was just clicked
        var view_state="";
        if(jQuery(this).attr("aria-expanded")=="true") view_state = "Expand List";
            else if(jQuery(this).attr("aria-expanded")=="false") view_state = "Collapse List";
        //alert("Section: "+jQuery(this).text().trim()+" ("+view_state+")");
        frn_reporting('','Content Interactions', 'Accordian Lists', "Section: "+jQuery(this).text().trim()+" ("+view_state+")", false, '');
    });
});

jQuery(document).ready(function(){

    if(jQuery(window).width() < 1024){
    	jQuery('#example  li').has('ul').addClass('dropdown');
        setTimeout(function(){ jQuery('#example  li').has('ul').prepend('<span></span>'); }, 4000);

        jQuery('.dropdown:not(.submenu_class) > a').on('click', function(e) {
            if( jQuery(this).siblings('ul').length ){
                e.preventDefault();
                if(!jQuery(this).nextAll('ul').is(':visible')){
                    jQuery('#example > li  ul').slideUp();  
                    jQuery('#example  li').removeClass('open');  
                    jQuery('#example  li  > span').removeClass('open');  
                    jQuery(this).nextAll('ul').slideDown();
                    jQuery(this).parent('li').addClass('open');
                    jQuery(this).addClass('open');
                    //alert("Sub LIs ARE NOW visible: "+jQuery(this).text());
                    frn_reporting('','Mobile Menu', 'Sub-Menu Opens', jQuery(this).text(), false, '');
                }else{
                    jQuery(this).nextAll('ul').slideUp();
                    jQuery(this).parent('li').removeClass('open');
                    jQuery(this).removeClass('open');
                    //alert("Sub LIs are now NOT visible: "+jQuery(this).text());
                    frn_reporting('','Mobile Menu', 'Sub-Menu Closes', jQuery(this).text(), false, '');
                }
                
            }
        });

        // jQuery('#example > li  > span').on('click',function(){

        //     if(!jQuery(this).nextAll('ul').is(':visible')){
        //         jQuery('#example > li  ul').slideUp();  
        //         jQuery('#example  li').removeClass('open');  
        //         jQuery('#example  li  > span').removeClass('open');  
        //         jQuery(this).nextAll('ul').slideDown();
        //         jQuery(this).parent('li').addClass('open');
        //         jQuery(this).addClass('open');
        //     }else{
        //         jQuery(this).nextAll('ul').slideUp();
        //         jQuery(this).parent('li').removeClass('open');
        //         jQuery(this).removeClass('open');
        //     }
        // })


        // jQuery('#example > li > ul > li > span').on('click',function(){
        // if(!jQuery(this).nextAll('ul').is(':visible')){
        // jQuery('#example > li > ul > li > ul').slideUp();  
        // jQuery('#example > li > ul > li').removeClass('open');  
        // jQuery('#example > li > ul > li > span').removeClass('open');  
        //  jQuery(this).nextAll('ul').slideDown();
        // jQuery(this).parent('li').addClass('open');
        //  jQuery(this).addClass('open');
        // }else{
        // jQuery(this).nextAll('ul').slideUp();
        // jQuery(this).parent('li').removeClass('open');
        //  jQuery(this).removeClass('open');
        // }
        // })

        // jQuery('#example > li > ul > li > ul > li > span').on('click',function(){

        // if(!jQuery(this).nextAll('ul').is(':visible')){
        // jQuery('#example > li > ul > li > ul > li > ul').slideUp();  
        // jQuery('#example > li > ul > li > ul > li').removeClass('open');  
        // jQuery('#example > li > ul > li > ul > li span').removeClass('open');  
        //  jQuery(this).nextAll('ul').slideDown();
        // jQuery(this).parent('li').addClass('open');
        //  jQuery(this).addClass('open');
        // }else{
        // jQuery(this).nextAll('ul').slideUp();
        // jQuery(this).parent('li').removeClass('open');
        //  jQuery(this).removeClass('open');
        // }
        // })

    }

});

jQuery(window).load(function() {
    jQuery('.ubermenu-submenu-align-full_width').each(function(){
        jQuery(this).find('li').wrapAll('<div class="row"></div>');
    });
    jQuery('.ubermenu-main ul.ubermenu-nav > li.ubermenu-item-has-children, .top-bar .main-menu > li.is-dropdown-submenu-parent  ').on('mouseover', function() {
		jQuery('header.site-header').addClass(' ubermenu-header-active');
    });
    jQuery('.ubermenu-main ul.ubermenu-nav > li.ubermenu-item-has-children, .top-bar .main-menu > li.is-dropdown-submenu-parent').on('mouseout', function() {
        jQuery('header.site-header').removeClass(' ubermenu-header-active');
    });
});

//Adds responsive embed to all iframes from youtube
jQuery(document).ready(function($) {

    var vidDefer = document.getElementsByTagName('iframe');
    // Remove empty P tags created by WP inside of Accordion and Orbit
    $('.accordion p:empty, .orbit p:empty').remove();
    
    $("p").find("iframe").unwrap();
    
    for (var i=0; i<vidDefer.length; i++) {
    if(vidDefer[i].getAttribute('data-src')) {
    vidDefer[i].setAttribute('src',vidDefer[i].getAttribute('data-src'));
    }
    }
    
    if(!$('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').parents("div").hasClass("sl_reveal")) {
    $('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').each(function() {
    if ( $(this).innerWidth() / $(this).innerHeight() > 1.5 ) {
    $(this).wrap("<div class='widescreen responsive-embed'/>");
    } else {
    $(this).wrap("<div class='responsive-embed'/>");
    }
    })
    }
});



//Mailchimp Email Form scripts. The markup can be found in /parts/contetn-emailform.php
jQuery(document).ready(function($) {
    var expanded = false;

    //Checks if dropdown is open and displays or hides accordingly
    function showCheckboxes(emailNumber) {
      var checkboxes = document.getElementById("sl_form__multiselect__options--" + emailNumber);

      console.log("show", checkboxes);
      if (!expanded) {
        checkboxes.style.display = "block";
        expanded = true;
      } else {
        checkboxes.style.display = "none";
        expanded = false;
      }
    };

    //Just hides the dropdown if it's open
    function hideCheckboxes(emailNumber) {
      var checkboxes = document.getElementById("sl_form__multiselect__options--" + emailNumber);

        checkboxes.style.display = "none";
        expanded = false;
    };

    $(".sl_form__multiselect__field").focus(function(e) {
      var emailNumber = e.target.id.split("--").pop().toString();
      var optionDropdown = document.getElementById("sl_form__multiselect__field--" + emailNumber);
      var emailField = document.getElementById("mce-EMAIL--" + emailNumber);

      showCheckboxes(emailNumber);

      if (document.activeElement === optionDropdown){
        $(optionDropdown).blur();
      }

      $(emailField).on('focus', function() {
       hideCheckboxes(emailNumber);
      })

      var selectOptions = '#sl_form__multiselect__options--' + emailNumber + ' input:checkbox'

      console.log("logs", selectOptions);

    $(selectOptions).on("change", function(){
      var whoOptions = [];
      $(selectOptions).each(function(){
       if($(this).is(":checked")){
         whoOptions.push(this.parentNode.textContent)
         document.getElementById("sl_who-input--" + emailNumber).innerHTML = whoOptions.concat();
         document.getElementById("sl_who-input--" + emailNumber).style.color = "#152435";
       }

       //If more than one option is selected
       if(whoOptions.length > 1){
        document.getElementById("sl_who-input--" + emailNumber).innerHTML = "Multiple Selected";
        document.getElementById("sl_who-input--" + emailNumber).style.color = "#152435";
       }

       //If less than one option is selected, reformat to initial state
       if(whoOptions.length < 1 || whoOptions == undefined){
        document.getElementById("sl_who-input--" + emailNumber).innerHTML = "I am a...";
        document.getElementById("sl_who-input--" + emailNumber).style.color = "inherit";
       }
      })   
    });
    });
});
//END MAILCHIMP JS

