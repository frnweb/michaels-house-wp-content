<?php
get_header();
?>

 <div class="main-content">
        <!-- Innerpage Banner -->
        <section class="banner banner-innerpage">
            <!-- Innerpage Banner Caption -->
            <div class="caption">
                <h1>Michael's House Staff</h1>
            </div>
        </section>
<div class="member-detail">
            <div class="row">
                <!-- Left Section -->
                <div class="medium-9 columns">
                   <div class="row">
                        <div class="medium-4 columns staff">
                        <?php $feature_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );if($feature_image){?>
                            <figure class="rounded">
                                <img src="<?php echo $feature_image;?>" alt="joseph">
                            </figure>
                            <?php }?>
                        </div>
                        <div class="medium-8 columns">
                        
                            <small><?php the_field('designation');?></small>
                            <h4><?php the_title();?></h4>
                            <?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
                            <?php the_content();?>
                            <?php endwhile; endif;?>
                        </div>
                    </div>
                  
                </div>
                
                <!-- Sidebar Section -->
                <aside class="medium-3 columns">
                   
                    <?php dynamic_sidebar('sidebar-1');?>
                </aside>
            </div>
        </div>
        </div>
<?php get_footer();?>