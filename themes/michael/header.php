<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="shortcut icon" href="<?php echo ot_get_option("favicon"); ?>" type="image/x-icon">
	<link rel="stylesheet" href="https://use.typekit.net/ibz5gcx.css">

	<?php wp_head(); ?>
	<meta property="og:image" content="http://www.michaelshouse.com/wp-content/uploads/Michaels-House-Banner-for-Social.jpg" />
	<meta property="og:image:width" content="1200" />
	<meta property="og:image:height" content="717" />

	<!-- VOB reCAPTCHA CDN -->
	<script src="https://www.google.com/recaptcha/api.js"></script>

	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NJ7T58X');</script>
<!-- End Google Tag Manager -->

<!-- Cloudflare Verify -->
<meta name="cf-2fa-verify" content="4d3443579a18a4f">
<!-- End Cloudflare Verify -->

<script
    type="text/javascript"
    async defer
    src="//assets.pinterest.com/js/pinit.js"
></script>

</head>

<body <?php if(is_single()): body_class("archive"); else: body_class(); endif; ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NJ7T58X"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- Begin Constant Contact Active Forms -->
<script> var _ctct_m = "07126172f9372044fb5e3928a2d167c5"; </script>
<script id="signupScript" src="//static.ctctcdn.com/js/signup-form-widget/current/signup-form-widget.min.js" async defer></script>
<!-- End Constant Contact Active Forms -->

    <div class="off-canvas-wrapper">

        <?php get_template_part( 'parts/header', 'offcanvas-menu' ); ?> 

        <div class="off-canvas-content" data-off-canvas-content>

             <!-- Health Alert Notification -->
            <div data-closable class="sl_notification-bar" id="sl_notification-bar" style="background-color:#242e3e;color:#ffffff; padding: 8px 24px;">
                <style type="text/css">
                    .sl_notification-bar .sl_inner { width: 100%; max-width: 1080px; margin: 0 auto; font-size: 18px; text-align: center;}
                    .sl_notification-bar .sl_button { display: inline-block; padding: 6px 10px; font-size: 15px; border-radius: 3px; color: #fff!important; text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2); background-color: #f26727; letter-spacing: 0; font-weight: 400; margin-left: 8px; margin-bottom: 0;}
                </style>
                <div class="sl_inner">
                    <span>Committed to Safety: Latest information on COVID-19 Precautions</span><a href="/important-covid-19-update-from-michaels-house/" class="sl_button">Learn More</a>
                </div>
                    <button class="close-button" aria-label="Close alert" style="color: #fff;" type="button" data-close>
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <!-- Health Alert Notification End -->

            <!-- Header-->
            <header class="site-header sl_header" id="masthead">
            	<?php get_template_part( 'parts/header', 'menu' ); ?>
            </header>
            <!-- Header Ends -->

            <!-- Info Button -->

            <!-- sticky CTA on bottom right of page -->
            <?php get_template_part( 'parts/header', 'sticky-cta' ); ?> 
