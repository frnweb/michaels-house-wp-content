<aside role="complementary" class="large-3 columns">	
<?php if (is_home() || is_single()) {?>
		
<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Default Sidebar') ) : ?>
	<?php include(TEMPLATEPATH . "/library/includes/5-steps-side.php");?>
	<?php endif; ?>
		
<?php } elseif (is_page('contact') || $post->post_parent == '2') {	?>
	
	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Default Sidebar') ) : ?>
	<?php endif; ?>


<?php } elseif (is_page('articles')) {	?>
	<?php get_search_form(); ?>
	<?php include(TEMPLATEPATH . "/library/includes/5-steps-side.php");?>
	<?php include(TEMPLATEPATH . "/library/includes/further-reading.php");?>
	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Default Sidebar') ) : ?>
	<?php endif; ?>

<?php } else { ?>
	<?php include(TEMPLATEPATH . "/library/includes/5-steps-side.php");?>
	<?php include(TEMPLATEPATH . "/library/includes/further-reading.php");?>
	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Default Sidebar') ) : ?>
	<?php endif; ?>

<?php } ?>
</aside>