<div class="row social-share">
	<div class="small-12 columns">
		<ul class="inline-list social-share-row right">
			<li><a class="fb-share label radius" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo the_permalink(); ?>">Share</a></li>
			<li><a class="twt-share label radius" target="_blank" href="https://twitter.com/intent/tweet?text=<?php echo get_the_title(); ?>&url=<?php echo the_permalink(); ?>&via=michaelshouse">Tweet</a></li>
			<li><a class="gplus-share label radius" target="_blank" href="https://plus.google.com/share?url=<?php echo the_permalink(); ?>">Share</a></li>
			<li><a class="li-share label radius" target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo the_permalink(); ?>&title=<?php echo get_the_title(); ?>&summary=<?php the_excerpt_rss(); ?>&source=YOUR-URL">Share</a></li>
		</ul>
	</div>
</div>