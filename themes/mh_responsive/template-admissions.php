<?php
/*
Template Name: Admissions OLD
*/
?>


<?php get_header(); ?>
<section class="banner admissions">
	<h1 class="text-center">Admissions<br><small>Trusted, Proven, Confidential </small></h1>
	<?php include get_template_directory().'/social.php'; ?>
</section>
<section class="spacing">
	<div class="row">
		<div class="medium-4 columns">
			<h2 class="text-center alt-h2">Initial Conversation</h2>
			<p>Here are some things that an admissions coordinator may discuss with you: How you or your loved one will travel to and from treatment, your insurance policy and what benefits may help cover the costs of treatment, recommended programs, length of stay and accommodations, and the help of a family mediator, especially if you are calling on behalf of a loved one.</p>
		</div>
		<div class="medium-4 columns">
			<h2 class="text-center alt-h2">Insurance</h2>
			<p>We accept most major insurance plans, and we will work with your insurance company to verify what benefits will cover your treatment expenses. Our insurance verification team works very hard to get the best payment plan to fit your needs. We will be able to give you a primary assessment of your options and what is covered or what costs may need to be paid out-of-pocket. The verification process does not cost you anything and will simply give you an appropriate picture of how you can plan for treatment financially. Please call us to learn more.</p>
				<div class="text-center">
					<a href="http://www.michaelshouse.com/insurance/" class="alt-link">How much will treatment cost?</a>
				</div> 
		</div>
		<div class="medium-4 columns">
			<h2 class="text-center alt-h2">Your Arrival</h2>
			<p>When you arrive at Michael’s House, an intake coordinator will sit down with you to assess your needs and create a plan for your treatment process. Here are a few things you may talk to your intake coordinator about:</p>
				<ul class="checks">
					<li>Your family history and medical background</li>
					<li>Your health and the drug of choice</li>
					<li>Your personal concerns and goals</li>
				</ul>
			<p>After the assessment, the intake coordinator will help you adjust to your new surroundings. You may go on a tour of the facility, meet your treatment team or simply relax in your room.</p>
			<div class="text-center">
					<ul class="inline-list">
						<li><a href="http://www.michaelshouse.com/treatment-admissions/what-to-bring/" class="alt-link">What to bring</a></li>
						<li><a href="http://www.michaelshouse.com/treatment-admissions/client-schedule/" class="alt-link">Schedule</a></li>
					</ul>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="light-block cta-spacing">
			<div class="row">
				<div class="small-12 columns text-center">
					<a href="http://www.michaelshouse.com/contact-us" class="button large round secondary"><span class="step">Step 5</span><span class="step-text">Contact us now to get started</span></a>
					<p class="helper">Learn all you need to know through a few easy steps</p>
				</div>
			</div>
		</div>
</section>
<section class="last">
	<div class="border-block spacing">
		<div class="row">
			<div class="medium-8 columns">
				<h2 id="call" class="text-center">Call</h2>				
			</div>
			<div class="medium-4 columns text-center right">
				<img src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/style/images/initial-convo.png">
			</div>
			<div class="medium-8 columns left">
				<p>If you would like to learn more about any part of our treatment program or how to get admitted to Michael’s House, please call our toll-free number.</p>
				<p>An admissions coordinator is ready to answer your questions and help you find the solution you’re looking for. Whether you’re seeking treatment for yourself or a loved one, we can help. Call us today.</p>
			</div>
		</div>
	</div>
	<div class="border-block spacing">
		<div class="row">
			<div class="medium-8 columns right">
				<h2 id="plan" class="text-center">Treatment Planning</h2>
			</div>
			<div class="left medium-4 columns text-center">
				<img src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/style/images/treatment-planning.png">
			</div>
			<div class="medium-8 columns right">
				<p>Treatment planning begins from your initial call and develops as the patient enters treatment. According to the needs of the individual, a clinical fit will be determined and follow-up assessments will be conducted to personalize that patient's plan.</p>
				<p>When the patient enters treatment, he or she will partner with staff members to set goals and get acquainted with the recovery process. Your needs are unique, so your plan is unique. We will walk with you on this journey one step at a time.</p>
			</div>
		</div>
	</div>
	<div class="border-block spacing">
		<div class="row">
			<div class="medium-8 columns left">
				<!-- <h2 class="text-center">Your Arrival</h2> -->
				<h2 id="detox" class="text-center">Detox</h2>
			</div>
			<div class="right medium-4 columns text-center">
				<img src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/style/images/mh-arrival.png">
			</div>
			<div class="medium-8 columns left">
				<p>Detox is the natural process that a person's body undergoes in the absence of an addictive substance. During this time, patients may feel withdrawal symptoms as their bodies adjust and regain chemical equilibrium.</p>
				<p>Detox at Michael's House is supervised by consulting physicians who will help ease the process and monitor patients to address any medical concerns as they arise. Once patients are free of the harmful toxins left over from substance abuse, their bodies and minds can begin to heal.</p>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>