<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<title><?php wp_title(''); ?></title>

<!-- dns prefetch -->
<link href="//www.google-analytics.com" rel="dns-prefetch">

<!-- meta -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="format-detection" content="telephone=no">
<meta property="og:image" content="http://www.michaelshouse.com/wp-content/uploads/Michaels-House-Banner-for-Social.jpg" />
<meta property="og:image:width" content="1200" />
<meta property="og:image:height" content="717" />
<meta name="twitter:image" content="http://www.michaelshouse.com/wp-content/uploads/Michaels-House-Banner-for-Social.jpg" />

<!-- icons -->
<link href="http://www.michaelshouse.com/wp-content/themes/mh_responsive/style/images/icons/favicon.ico" rel="shortcut icon">
	
<!-- css + javascript -->
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic|Rock+Salt">
<script src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/library/js/modernizr.min.js"></script>
<?php wp_head(); ?>

<!-- Hotjar updated by Matt 3.17.2015 -->
<script>
    (function(f,b){
        var c;
        f.hj=f.hj||function(){(f.hj.q=f.hj.q||[]).push(arguments)};
        f._hjSettings={hjid:1228, hjsv:3};
        c=b.createElement("script");c.async=1;
        c.src="//static.hotjar.com/c/hotjar-1228.js?sv=3";
        b.getElementsByTagName("head")[0].appendChild(c); 
    })(window,document);
</script>
<!-- Hotjar updated by Matt 3.17.2015 -->


</head>
<body <?php body_class(); ?>>

<!-- header -->

<header class="light">
	<div class="row">
		<div class="large-5 medium-8 columns">
	    	<!--Mobile Menu Sidr-->
	    	<div class="hide-for-medium-up left">
	    		<a id="responsive-menu-button" href="#sidr-main">Menu</a>
	    		<div id="mobile-title" class="hide"><h1>Michael's House</h1></div>
	    	</div>
	    	<!-- logo -->
			<div class="logo">
				<a href="<?php echo home_url(); ?>" title="">
					<img data-interchange="[http://www.michaelshouse.com/wp-content/themes/mh_responsive/style/images/mh-logo-retina.png, (retina)]" src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/style/images/mh-logo.png" alt="Michael's House" width="125px" height="56px">
				</a>				
			</div>
			<!-- /logo -->
			<!-- tagline -->
			<p class="tagline hide-for-small-only"><?php echo get_bloginfo( 'description' ); ?></p> 
			<!-- /tagline -->
		</div>
		<div class="large-7 medium-4 columns hide-for-small-only">
			<div class="right">
				<div class="phone-header text-center">
					<span class="phone"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Header]'); ?></span><br>
					<a href="#" data-reveal-id="what2expect" onClick="ga('send', 'event', 'Popups', ' What to Expect Link in Header ','Open');">What to expect when you call »</a>
					<div class="reveal-modal medium" id="what2expect" data-reveal>
						<h3>Making the Call</h3>
						<p>Here are some of the topics our Admissions Coordinator may discuss with you during the initial conversation:</p> 
						<ul class="checks">
							<li>Travel arrangements for you or your loved one to and from treatment</li>
							<li>Your insurance policy and what benefits may help cover the cost of treatment. Michael's House accepts many private insurances and can also work with you on a payment plan.</li>
							<li>Recommended programs, length of stay and accommodations.</li>
							<li>Getting help from a sober escort or family mediator, especially if you are calling on behalf of a loved one.</li>
						</ul>
						<p class="text-center orange phone-cta"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Header (What to Expect)"]'); ?></p><br>
						<div class="flex-video">
							<iframe width="1280" height="720" src="//www.youtube.com/embed/1mYxbj1NW38?rel=0" frameborder="0" allowfullscreen></iframe>
						</div>
						<p>We care about your unique situation and your needs. At Michael's House, our goal is to provide lifetime recovery solutions.</p>
					</div>
				</div>
			</div>
			<div class="right show-for-large-up">
				<nav id="top-nav" role="navigation"><?php wp_nav_menu( array( 'theme_location' => 'top-nav' ) ); ?></nav>						
			</div>
		</div>
	</div>		
</header>
<!-- end header-container -->
<!-- Main Nav Bar -->
<nav id="main-nav" role="navigation">
	<div class="row">		
			<div class="small-11 columns hide-for-small-only no-pad"><?php wp_nav_menu( array( 'theme_location' => 'main-nav' ) ); ?></div>
			<div class="small-1 columns hide-for-small-only"><a href="#" id="menuIcon" class="left" onClick="ga('send', 'event', 'Hamburger Menu', 'Menu Opens & Closes'); "><img src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/style/images/hamburger-dropdown.png"></a></div>
	</div>
</nav>	
<section id="bigNav" style="display:none">
	<div class="row">
		<div class="large-3 medium-6 columns">
			<h4>About</h4>
			<?php wp_nav_menu( array( 'theme_location' => 'sub-nav1' ) ); ?>
		</div> 
		<div class="large-3 medium-6 columns">
			<h4>Treatment & Mental Health</h4>
			<?php wp_nav_menu( array( 'theme_location' => 'sub-nav2' ) ); ?>
		</div> 
		<div class="large-3 medium-6 columns">
			<h4>Locations</h4>
			<?php wp_nav_menu( array( 'theme_location' => 'sub-nav3' ) ); ?>
			<h4>Quick Links</h4>
			<?php wp_nav_menu( array( 'theme_location' => 'sub-nav4' ) ); ?>
		</div> 
		<div class="large-3 medium-6 columns">
			<h4>Common Issues</h4>
			<?php wp_nav_menu( array( 'theme_location' => 'sub-nav5' ) ); ?>
		</div>
	</div>
</section>		
<!-- end Main Nav Bar -->