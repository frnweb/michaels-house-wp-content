<div id="mobile-footer" class="small-12 columns call-block text-center hide-for-large-up">
    <h3><small>A Better Tomorrow Starts Today</small><br><span class="orange"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Floating Footer"]'); ?></span></h3>
</div>
<footer>
    <div class="palms"></div>
    <div class="dark">
    	<div class="row">
    		<div class="small-12 columns call-block text-center">
    			<h3>Speak with an Admissions Coordinator <span class="orange"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Footer"]'); ?></span><br><small>A Better Tomorrow Starts Today</small></h3><br>
    			<div class="small-6 small-centered columns text-center">
                    <img data-interchange="[http://www.michaelshouse.com/wp-content/themes/mh_responsive/style/images/mh-logo-retina.png, (retina)]" src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/style/images/mh-logo.png" alt="Michael's House" width="125px" height="56px">
                </div>
    		</div>
    	</div>
    	<div class="row">
    		<div class="large-3 medium-6 small-12 columns">
    			<?php dynamic_sidebar( 'footer-menu-1' ); ?>
    		</div>
    		<div class="large-3 medium-6 small-12 columns">
    			<?php dynamic_sidebar( 'footer-menu-2' ); ?>
    		</div>
    		<div class="large-3 medium-6 small-12 columns">
    			<?php dynamic_sidebar( 'footer-menu-3' ); ?>
    		</div>
    		<div class="large-3 medium-6 small-12 columns">
    			<?php dynamic_sidebar( 'footer-menu-4' ); ?>
    		</div>
    	</div>
    	<div class="row">
    		<div class="large-3 small-12 columns social-block">
    			<h4>Connect & Share</h4>
    			<ul class="social">
    				<li><a href="https://www.facebook.com/michaelshousepalmsprings" class="facebook" target="_blank"></a></li>
    				<li><a href="https://twitter.com/michaelshouse" class="twitter" target="_blank"></a></li>
    				<li><a href="https://www.youtube.com/watch?v=Z_qdDO-RUoU&list=PL506A0DFDEF35398F" class="youtube" target="_blank"></a></li>
    				<li><a href="https://plus.google.com/+MichaelsHousePalmSprings" class="gplus" target="_blank"></a></li>
    			</ul>
    		</div>
    		<div class="large-9 small-12 columns network-block">
    			<h4>Meet our Network</h4>
    			<ul class="small-block-grid-3 medium-block-grid-3 large-block-grid-5">
    				<li><a href="http://www.foundationsrecoverynetwork.com/" class="frn" target="_blank"></a></li>
    				<li><a href="http://thecanyonmalibu.com/" class="cyn" target="_blank"></a></li>
    				<li><a href="http://www.theoakstreatment.com/" class="oaks" target="_blank"></a></li>
    				<li><a href="http://blackbearrehab.com/" class="bb" target="_blank"></a></li>
    				<li><a href="http://www.dualdiagnosis.org/" class="dd" target="_blank"></a></li>
    			</ul>
    		</div>
    	</div>
    </div>


    <div id="copyright">
        <div class="row">
            <div class="small-8 columns">
                <div class="left">
                    <?php echo do_shortcode('[frn_footer align="left"]'); ?>                    
                </div>
            </div>
            <div class="small-4 columns">
                <div class="right">
                    <a href="http://www.bbb.org/los-angeles/business-reviews/alcoholism-info-and-treatment-centers/michaels-house-in-palm-springs-ca-13049282" target="_blank"><img src="http://www.bbb.org/los-angeles/images/2/cbbb-badge-horz.png" WIDTH="135" HEIGHT="52" BORDER="0"></a>
                </div>
            </div>
        </div>        
    </div>
</footer>

<?php wp_footer(); ?>
<script type="text/javascript" src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/library/js/sidr-foundation.min.js"></script>
<script type="text/javascript" src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/library/js/scripts.js"></script>
  <script>
    $(document).foundation();
  </script>

<?php
if ( wp_is_mobile() ) {?>
    <div id="mobileCTA" class="reveal-modal" data-reveal>
        <h3 class="text-center">Speak to a Professional</h3>
        <p class="text-center orange phone-cta"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Mobile Lightbox"]'); ?></p>        
        <p>We care about your unique situation and needs. At Michael's House, our goal is to provide lifetime recovery solutions.</p>
    </div>
    <script type="text/javascript" src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/library/js/jquery.cookie.js"></script>
    <script>
        $(document).ready(function() {
            if ($.cookie('modal_shown') == null) {
                $.cookie('modal_shown', 'yes', { expires: 7, path: '/' });
                setTimeout(function(){
                    $('#mobileCTA').foundation('reveal', 'open');
            }, 4000);
        }
    });
    </script>
<?php } ?>

    <script type="text/javascript">
        (function(){
          function initInsights() {
            if (!window._contently) {
              var _contently = { siteId: "b58567b5992bf3eea309" }
              _contently.insights = new ContentlyInsights({site_id: _contently.siteId});
              window._contently = _contently
            }
          }

          var s = document.createElement('script');
          s.setAttribute('type', 'text/javascript');
          s.setAttribute('src', '//s3.amazonaws.com/assets.contently.com/insights/insights.js')
          s.onreadystatechange = function() {
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
              initInsights();
            }
          };
          s.onload = initInsights;
          document.body.appendChild(s);
        })();
    </script>

    <script type="text/javascript">
        var _stk = "29b7c6a9512f3b1450d85acb9aa55fb8818ba6aa";
        (function(){
            var a=document, b=a.createElement("script"); b.type="text/javascript";
            b.async=!0; b.src=('https:'==document.location.protocol ? 'https://' :
            'http://') + 'd31y97ze264gaa.cloudfront.net/assets/st/js/st.js';
            a=a.getElementsByTagName("script")[0]; a.parentNode.insertBefore(b,a);
        })();
    </script>

</body>
</html>
