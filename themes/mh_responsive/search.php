<?php get_header(); ?>
	<?php if (have_posts()) : ?>
	<section class="light-block standard-header">
		<div class="row">
			<div class="small-12 columns">
				<header id="page-id">
					<h1>Search Results for "<?php echo $search_query = get_search_query(); ?>"</h1>
				</header>
			</div>
		</div>
	</section>		
	<div class="row last">
	<section role="main" class="large-9 columns">			
		<?php while (have_posts()) : the_post(); ?>
		<article>
			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">

				<h2><a href='<?php echo get_permalink($post->ID)?>'><?php the_title(); ?></a></h2>

				<div class="entry">

					<?php the_excerpt(); ?>

				</div>

			</div>
			<hr>
		</article>
		<?php endwhile; ?>


	<?php else : ?>

	<section class="light-block standard-header">
		<div class="row">
			<div class="small-12 columns">
				<header id="page-id">
					<h1>No posts found for "<?php echo $search_query = get_search_query(); ?>"</h1>
				</header>
			</div>
		</div>
	</section>

	<?php endif; ?>
	</section>
<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>
