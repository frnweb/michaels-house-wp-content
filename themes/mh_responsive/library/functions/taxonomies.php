<?php
// -------------------------------  Taxonomies ---------------------------------------

// Staff Location
register_taxonomy(  
	'staff_location',  
	array('staff'),  
	array(  
	 'hierarchical' => true,  
	 'label' => 'Staff Location',  
	 'query_var' => true,  
	 'rewrite' => true  
	)  
);  

?>