<?php
// function post_type_sample() {
//     $labels = array(
//         'name' => __( 'Sample' ),
//         'singular_name' => __( 'Sample' ),
//         'add_new' => __( 'Add New Sample' ),
//         'add_new_item' => __( 'Add New Sample' ),
//         'edit_item' => __( 'Edit Sample' ),
//         'new_item' => __( 'Add New Sample' ),
//         'view_item' => __( 'View Sample' ),
//         'search_items' => __( 'Search Samples' ),
//         'not_found' => __( 'No Samples found' ),
//         'not_found_in_trash' => __( 'No Samples found in trash' )
//     );
//     $args = array(
//         'labels' => $labels,
//         'public' => true,
//         'supports' => array( 'title', 'editor', 'thumbnail', 'comments' ),
//         'capability_type' => 'post',
//         'rewrite' => array("slug" => "Sample"), // Permalinks format
//         'menu_position' => 5,
//         //'menu_icon' => plugin_dir_url( __FILE__ ) . '/images/calendar-icon.gif',  // Icon Path
//         'has_archive' => true
//     );
//     register_post_type( 'sample', $args );
// }
// add_action( 'init', 'post_type_sample' );
// Staff
function post_type_staff() {
    $labels = array(
        'name' => __( 'Staff' ),
        'singular_name' => __( 'Staff' ),
        'add_new' => __( 'Add New Staff' ),
        'add_new_item' => __( 'Add New Staff' ),
        'edit_item' => __( 'Edit Staff' ),
        'new_item' => __( 'Add New Staff' ),
        'view_item' => __( 'View Staff' ),
        'search_items' => __( 'Search Staff' ),
        'not_found' => __( 'No Staff found' ),
        'not_found_in_trash' => __( 'No Staff found in trash' )
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'supports' => array( 'title', 'thumbnail', 'editor', 'custom-fields' ),
        'capability_type' => 'post',
        'rewrite' => array('with_front' => false, "slug" => "staff"), // Permalinks format 
        'menu_position' => 5,
        //'menu_icon' => plugin_dir_url( __FILE__ ) . '/images/calendar-icon.gif',  // Icon Path
        'has_archive' => true
    );
    register_post_type( 'staff', $args );
}
add_action( 'init', 'post_type_staff' );
?>