<?php
/*
Template Name: Admissions Alt
*/
?>


<?php get_header(); ?>
<section class="banner admissions">
	<h1 class="text-center">Admissions<br><small>Trusted, Proven, Confidential </small></h1>
	<div class="row" data-equalizer>
		<div class="large-6 large-uncentered medium-8 medium-centered columns" data-equalizer-watch>
			<div class="flex-video">
				<iframe width="1280" height="720" src="//www.youtube.com/embed/1mYxbj1NW38?rel=0" frameborder="0" allowfullscreen></iframe>
			</div>
		</div>
		<div class="large-6 columns" data-equalizer-watch>
			<p>When you call, an admissions coordinator will answer the phone and take time to ask you some questions about what you are struggling with or what you are looking for.</p>
			<p>The admissions coordinator conducts this brief interview in order to best determine your treatment options and what type of rehabilitation is right for you. If you’re calling on behalf of a loved one, your coordinator will gather information about your loved one in order to find the treatment program that best fits him or her.</p>
			<div class="text-center">
				<a href="http://www.michaelshouse.com/contact-us" class="button large round secondary"><span class="step-text">Contact us now to get started</span></a>
				<p class="helper">Learn all you need to know through a few easy steps</p>
			</div>				
		</div>
	</div>
	<?php include get_template_directory().'/social.php'; ?>
</section>
<section class="spacing">
	<div class="row">
		<div class="medium-4 columns">
			<h2 class="text-center alt-h2">Initial Conversation</h2>
			<p>Here are some things that an admissions coordinator may discuss with you: How you or your loved one will travel to and from treatment, your insurance policy and what benefits may help cover the costs of treatment, recommended programs, length of stay and accommodations, and the help of a family mediator, especially if you are calling on behalf of a loved one.</p>
		</div>
		<div class="medium-4 columns">
			<h2 class="text-center alt-h2">Insurance</h2>
			<p>We accept most major insurance plans, and we will work with your insurance company to verify what benefits will cover your treatment expenses. Our insurance verification team works very hard to get the best payment plan to fit your needs. We will be able to give you a primary assessment of your options and what is covered or what costs may need to be paid out-of-pocket. The verification process does not cost you anything and will simply give you an appropriate picture of how you can plan for treatment financially. Please call us to learn more.</p>
				<div class="text-center">
					<a href="http://www.michaelshouse.com/insurance/" class="alt-link">How much will treatment cost?</a>
				</div> 
		</div>
		<div class="medium-4 columns">
			<h2 class="text-center alt-h2">Your Arrival</h2>
			<p>When you arrive at Michael’s House, an intake coordinator will sit down with you to assess your needs and create a plan for your treatment process. Here are a few things you may talk to your intake coordinator about:</p>
				<ul class="checks">
					<li>Your family history and medical background</li>
					<li>Your health and the drug of choice</li>
					<li>Your personal concerns and goals</li>
				</ul>
			<p>After the assessment, the intake coordinator will help you adjust to your new surroundings. You may go on a tour of the facility, meet your treatment team or simply relax in your room.</p>
			<div class="text-center">
					<ul class="inline-list">
						<li><a href="http://www.michaelshouse.com/treatment-admissions/what-to-bring/" class="alt-link">What to bring</a></li>
						<li><a href="http://www.michaelshouse.com/treatment-admissions/client-schedule/" class="alt-link">Schedule</a></li>
					</ul>
			</div>
		</div>
	</div>
</section>
<section class="cta-spacing light-block">
	<div class="row">
		<div class="medium-8 small-11 small-centered columns panel">
			<h2 class="text-center alt-h2">Intervention</h2>
			<p>An intervention isn't just a meeting. It's the strategic commitment to a loved one's recovery process, and it's just as much about getting the family stabilized as the person suffering from the addiction. When you want to help someone you love who is not yet seeking treatment, you can trust us to connect you to professional interventionist whose expertise matches your situation. You are one phone call away from getting the help of a family mediator who can restore balance to your family and help your loved one recognize the need for healing and restoration.</p>
			<div class="text-center"><p><a href="http://www.michaelshouse.com/intervention" class="alt-link">Intervention</a></p></div>
		</div>
	</div>
</section>
<section>
	<div class="border-block spacing">
		<div class="row">
			<div class="medium-8 columns">
				<h2 id="call" class="text-center">Call</h2>				
			</div>
			<div class="medium-4 columns text-center right">
				<img src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/style/images/initial-convo.png">
			</div>
			<div class="medium-8 columns left">
				<p>If you would like to learn more about any part of our treatment program or how to get admitted to Michael’s House, please call our toll-free number.</p>
				<p>An admissions coordinator is ready to answer your questions and help you find the solution you’re looking for. Whether you’re seeking treatment for yourself or a loved one, we can help. Call us today.</p>
			</div>
		</div>
	</div>
	<div class="border-block spacing">
		<div class="row">
			<div class="medium-8 columns right">
				<h2 id="plan" class="text-center">Treatment Planning</h2>
			</div>
			<div class="left medium-4 columns text-center">
				<img src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/style/images/treatment-planning.png">
			</div>
			<div class="medium-8 columns right">
				<p>Treatment planning begins from your initial call and develops as the patient enters treatment. According to the needs of the individual, a clinical fit will be determined and follow-up assessments will be conducted to personalize that patient's plan.</p>
				<p>When the patient enters treatment, he or she will partner with staff members to set goals and get acquainted with the recovery process. Your needs are unique, so your plan is unique. We will walk with you on this journey one step at a time.</p>
			</div>
		</div>
	</div>
	<div class="border-block spacing">
		<div class="row">
			<div class="medium-8 columns left">
				<!-- <h2 class="text-center">Your Arrival</h2> -->
				<h2 id="detox" class="text-center">Detox</h2>
			</div>
			<div class="right medium-4 columns text-center">
				<img src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/style/images/mh-arrival.png">
			</div>
			<div class="medium-8 columns left">
				<p>Detox is the natural process that a person's body undergoes in the absence of an addictive substance. During this time, patients may feel withdrawal symptoms as their bodies adjust and regain chemical equilibrium.</p>
				<p>Detox at Michael's House is supervised by consulting physicians who will help ease the process and monitor patients to address any medical concerns as they arise. Once patients are free of the harmful toxins left over from substance abuse, their bodies and minds can begin to heal.</p>
			</div>
		</div>
	</div>
</div>
</section>
<section class="last spacing light-block">
	<div class="row">
		<div class="medium-8 small-11 small-centered columns panel">
			<h2 class="text-center alt-h2">Exclusion for Admission Criteria</h2>
			<ol>
				<li>The individual exhibits suicidal or homicidal ideation with a plan and intent to carry out the plan, indicating a need for acute hospitalization</li>
				<li>Disruptive psychosis to the extent that the individual is unable to care for himself or is a threat to others</li>
				<li>When the patient is in need of urgent medical care due to detoxification or other medical issues</li>
				<li>Medical equipment needed to support the individual may be beyond the scope of a sub-acute facility</li>
				<li>The individual cannot safely navigate the environment of care for his own personal needs</li>
				<li>The individual does not voluntarily consent to admission or treatment</li>
			</ol>
		</div>
	</div>
</section>
<?php get_footer(); ?>