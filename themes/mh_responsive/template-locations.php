<?php
/*
Template Name: Our Locations
*/
?>


<?php get_header(); ?>
<section class="banner our-locations">
	<h1 class="text-center">Our Locations<br><small>Michael's House Campuses</small></h1>
	<div class="row" data-equalizer>
		<div class="large-6 large-uncentered medium-8 medium-centered columns" data-equalizer-watch>
			<div class="flex-video">
				<iframe width="1280" height="720" src="//www.youtube.com/embed/MahTEp3Pil0?rel=0" frameborder="0" allowfullscreen></iframe>
			</div>
		</div>
		<div class="large-6 columns" data-equalizer-watch>
			<p>Michael’s House has four campuses that are private and exclusive. All campuses are nationally recognized and internationally accredited by the Commission on Accreditation for Rehabilitation Facilities (CARF). We are located in the heart of downtown Palm Springs, which is known throughout the world for its special brand of relaxation, style and sophistication. Palm Springs has been a destination of choice for celebrities, movie executives and world leaders since the 1930s.</p>
			<div class="text-center">
				<a href="http://www.michaelshouse.com/treatment-admissions" class="button large round secondary"><span class="step">Step 5</span><span class="step-text">What Does the Admissions Process Look Like?</span></a>
				<p class="helper">Learn all you need to know through a few easy steps</p>
			</div>	
			<div class="text-center">
				<ul class="inline-list">
				<li><a href="#stabl" class="alt-link">Stabilization Center</a></li>
				<li><a href="#mens" class="alt-link">Men's Center</a></li>
				<li><a href="#womens" class="alt-link">Women's Center</a></li>
				<li><a href="#outpatient" class="alt-link">Outpatient Center</a></li>
			</ul>
			</div>
		</div>
	</div>
	<?php include get_template_directory().'/social.php'; ?>
</section>
<section>
	<div class="border-block spacing">
		<div class="row">
			<div class="large-6 columns">
				<a name="stabl"></a>
				<h2 class="text-center">Stabilization Center</h2>
			</div>
			<div class="large-6 large-reset-order medium-8 medium-pull-2 columns text-center right">
					<ul class="clearing-thumbs" data-clearing>
						<li><a href="http://www.michaelshouse.com/wp-content/themes/mh_responsive/style/images/stabl-center.jpg"><img class="frame-border-large" src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/style/images/stabl-center.jpg"></a></li>
						<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/michaels-house-stabilization.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/michaels-house-stabilization.jpg"></a></li>
						<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/michaels-house-stabilization2.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/michaels-house-stabilization2.jpg"></a></li>
						<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/michaels-house-stabilization3.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/michaels-house-stabilization3.jpg"></a></li>
						<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/michaels-house-stabilization4.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/michaels-house-stabilization4.jpg"></a></li>
						<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/michaels-house-stabilization5.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/michaels-house-stabilization5.jpg"></a></li>
						<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/michaels-house-stabilization6.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/michaels-house-stabilization6.jpg"></a></li>
						<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/michaels-house-stabilization7.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/michaels-house-stabilization7.jpg"></a></li>
						<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/michaels-house-stabilization8.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/michaels-house-stabilization8.jpg"></a></li>
						<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/michaels-house-stabilization10.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/michaels-house-stabilization10.jpg"></a></li>
						<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/michaels-house-stabilization11.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/michaels-house-stabilization11.jpg"></a></li>
						<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/michaels-house-stabilization12.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/michaels-house-stabilization12.jpg"></a></li>
					</ul>
				<!-- <div class="frame">
					<a href="">
						<img src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/style/images/stabl-center.jpg">
					</a>
				</div> -->
			</div>
			<div class="large-6 columns">
				<p>
					First impressions are important. From the moment patients arrive for their treatment experience, they will be greeted by friendly faces at Michael’s House Stabilization Center. Our dedicated team of intake coordinators knows how to make patients feel welcomed and taken care of from the very start. If patients are flying into Palm Springs from out of state, an intake coordinator will meet them at the airport.  The Stabilization Center is unique, modern building with a serene dining area, rooms surrounding the pool, lounge areas, group rooms and staff offices. It provides a cool environment in the warm Palm Springs weather, and it fosters a regimen of healing for patients seeking recovery.
				</p>
				<div class="text-center">
					<a href="http://www.michaelshouse.com/locations/michaels-house-north/" class="button large radius"><span class="arrow">Stabilization Center</span></a>
				</div>
			</div>
		</div>
		<!-- <div class="row">
			<div class="large-6 medium-8 columns">
				<a name="stabl"></a>
				<h2 class="text-center">Stabilization Center</h2>
				<p>
					First impressions are important. From the moment patients arrive for their treatment experience, they will be greeted by friendly faces at Michael’s House Stabilization Center. Our dedicated team of intake coordinators knows how to make patients feel welcomed and taken care of from the very start. If patients are flying into Palm Springs from out of state, an intake coordinator will meet them at the airport.  The Stabilization Center is unique, modern building with a serene dining area, rooms surrounding the pool, lounge areas, group rooms and staff offices. It provides a cool environment in the warm Palm Springs weather, and it fosters a regimen of healing for patients seeking recovery.
				</p>
				<div class="text-center">
					<a href="#" class="button large radius"><span class="arrow">Stabilization Center</span></a>
				</div>
			</div>
			<div class="large-6 medium-4 columns">
				<div class="frame">
					<a href="">
						<img src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/style/images/stabl-center.jpg">
					</a>
				</div>
			</div>
		</div> -->
	</div>
	<div class="border-block spacing">
		<div class="row">
			<div class="large-6 columns right">
				<a name="mens"></a>
				<h2 class="text-center">Men's Center</h2>
			</div>
			<div class="large-6 large-reset-order medium-8 medium-push-2 columns text-center left">
				<ul class="clearing-thumbs" data-clearing>
					<li><a href="http://www.michaelshouse.com/wp-content/themes/mh_responsive/style/images/mens-center.jpg"><img class="frame-border-large" src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/style/images/mens-center.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/michaels-house-walkway.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/michaels-house-walkway.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/034_HDR.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/034_HDR.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/036_HDR.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/036_HDR.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/023_HDR.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/023_HDR.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/032_HDR.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/032_HDR.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/015_HDR.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/015_HDR.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/018_HDR.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/018_HDR.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/008_HDR.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/008_HDR.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/007_HDR.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/007_HDR.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/005_HDR.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/005_HDR.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/003_HDR.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/003_HDR.jpg"></a></li>
				</ul>
				<!-- <div class="frame">
					<a href="">
						<img src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/style/images/mens-center.jpg">
					</a>
				</div> -->
			</div>
			<div class="large-6 columns left">
				<p>
					The Men’s Center at Michael’s House is a place where men can find solid ground in recovery. Its strength-based curriculum enables men to process and problem-solve, recognize positive and negative patterns, build character, find balance and establish life goals. The experienced staff at the Men’s Center facilitate patients’ renewal and recovery in a way that stabilizes and builds them up so that they are prepared to overcome challenges and respond to life with new coping skills and healthy behaviors. Our positive, affirming program paves the way for men to achieve and sustain long-term recovery.
				</p>
				<div class="text-center">
					<a href="http://www.michaelshouse.com/locations/mens-center" class="button large radius"><span class="arrow">Men's Center</span></a>
				</div>
			</div>
		</div>
		<!-- <div class="row">
			<div class="large-6 medium-4 columns">
				<div class="frame">
					<a href="">
						<img src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/style/images/mens-center.jpg">
					</a>
				</div>
			</div>
			<div class="large-6 medium-8 columns">
				<a name="mens"></a>
				<h2 class="text-center">Men's Center</h2>
				<p>
					The Men’s Center at Michael’s House is a place where men can find solid ground in recovery. Its strength-based curriculum enables men to process and problem-solve, recognize positive and negative patterns, build character, find balance and establish life goals. The experienced staff at the Men’s Center facilitate patients’ renewal and recovery in a way that stabilizes and builds them up so that they are prepared to overcome challenges and respond to life with new coping skills and healthy behaviors. Our positive, affirming program paves the way for men to achieve and sustain long-term recovery.
				</p>
				<div class="text-center">
					<a href="#" class="button large radius"><span class="arrow">Men's Center</span></a>
				</div>
			</div>
		</div> -->
	</div>
	<div class="border-block spacing">
		<div class="row">
			<div class="large-6 columns">
				<a name="womens"></a>
				<h2 class="text-center">Women's Center</h2>
			</div>
			<div class="large-6 large-reset-order medium-8 medium-pull-2 columns text-center right">
				<ul class="clearing-thumbs" data-clearing>
					<li><a href="http://www.michaelshouse.com/wp-content/themes/mh_responsive/style/images/womens-center.jpg"><img class="frame-border-large" src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/style/images/womens-center.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/womens-location1.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/womens-location1.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/womens-program1.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/womens-program1.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/Womens-facility1.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/Womens-facility1.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/womens-center2.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/womens-center2.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/poolside1.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/poolside1.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/peaceful-rest1.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/peaceful-rest1.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/michaels-house-womens1.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/michaels-house-womens1.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/our-entrance1.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/our-entrance1.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/hot-day-cool-off1.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/hot-day-cool-off1.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/evening-skies1.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/evening-skies1.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/gatherings1.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/gatherings1.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/center-for-women1.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/center-for-women1.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/comfortable-quarters2.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/comfortable-quarters2.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/beautiful-grounds1.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/beautiful-grounds1.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/womens-location.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/womens-location.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/womens-program.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/womens-program.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/Womens-facility.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/Womens-facility.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/womens-center1.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/womens-center1.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/michaels-house-womens.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/michaels-house-womens.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/center-for-women.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/center-for-women.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/loc-1.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/loc-1.jpg"></a></li>
				</ul>
				<!-- <div class="frame">
					<a href="">
						<img src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/style/images/womens-center.jpg">
					</a>
				</div> -->
			</div>
			<div class="large-6 columns">
				<p>
					At Michael’s House Women’s Center, women can participate in a positive treatment process that focuses on resiliency and motivational principles to empower them to achieve their goals in recovery. The Women’s Center is a place of healing and hope, with community areas, recreational activities and places for reflection and down time. The multi-disciplinary team at the Women’s Center is dedicated to the well-being of each individual, and they meet together continually to discuss the progress and best options for every patient—combining their skills and expertise for a truly integrated approach to treatment.
				</p>
				<div class="text-center">
					<a href="http://www.michaelshouse.com/locations/michaels-house-south" class="button large radius"><span class="arrow">Women's Center</span></a>
				</div>
			</div>			
		</div>
		<!-- <div class="row">
			<div class="large-6 medium-8 columns">
				<a name="womens"></a>
				<h2 class="text-center">Women's Center</h2>
				<p>
					At Michael’s House Women’s Center, women can participate in a positive treatment process that focuses on resiliency and motivational principles to empower them to achieve their goals in recovery. The Women’s Center is a place of healing and hope, with community areas, recreational activities and places for reflection and down time. The multi-disciplinary team at the Women’s Center is dedicated to the well-being of each individual, and they meet together continually to discuss the progress and best options for every patient—combining their skills and expertise for a truly integrated approach to treatment.
				</p>
				<div class="text-center">
					<a href="#" class="button large radius"><span class="arrow">Women's Center</span></a>
				</div>
			</div>
			<div class="large-6 medium-4 columns">
				<div class="frame">
					<a href="">
						<img src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/style/images/womens-center.jpg">
					</a>
				</div>
			</div>
		</div> -->
	</div>
	<div class="border-block spacing last">
		<div class="row">
			<div class="large-6 columns right">
				<a name="outpatient"></a>
				<h2 class="text-center">Outpatient Center</h2>
			</div>
			<div class="large-6 large-reset-order medium-8 medium-push-2 columns text-center left">
				<ul class="clearing-thumbs" data-clearing>
					<li><a href="http://www.michaelshouse.com/wp-content/themes/mh_responsive/style/images/outpatient-center.jpg"><img class="frame-border-large" src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/style/images/outpatient-center.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/michaels-house-outpatient8.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/michaels-house-outpatient8.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/michaels-house-outpatient7.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/michaels-house-outpatient7.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/michaels-house-outpatient6.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/michaels-house-outpatient6.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/michaels-house-outpatient5.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/michaels-house-outpatient5.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/michaels-house-outpatient4.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/michaels-house-outpatient4.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/michaels-house-outpatient3.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/michaels-house-outpatient3.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/michaels-house-outpatient2.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/michaels-house-outpatient2.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/michaels-house-outpatient.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/michaels-house-outpatient.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/outpatient9.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/outpatient9.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/outpatient8.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/outpatient8.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/outpatient7.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/outpatient7.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/outpatient6.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/outpatient6.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/outpatient5.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/outpatient5.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/outpatient4.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/outpatient4.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/outpatient3.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/outpatient3.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/outpatient2.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/outpatient2.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/outpatient-center.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/outpatient-center.jpg"></a></li>
					<li class="hide"><a href="http://www.michaelshouse.com/wp-content/uploads/outpatient-location2.jpg"><img class="frame-border" src="http://www.michaelshouse.com/wp-content/uploads/outpatient-location2.jpg"></a></li>
				</ul>
			</div>
			<div class="large-6 columns right">				
				<p>
					Our outpatient center features three levels that build on the levels system used in our residential program. Patients begin with three hours of education per day, focusing on developing a schedule for a life of sobriety, increasing their recovery skill sets, continuing in group therapy, and gaining valuable relapse prevention awareness.
				</p>
				<div class="text-center">
					<a href="http://www.michaelshouse.com/locations/outpatient-program" class="button large radius"><span class="arrow">Outpatient Center</span></a>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>