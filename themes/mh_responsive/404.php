<?php get_header(); ?>
<section class="light-block standard-header">
	<div class="row">
		<div class="small-12 columns">
			<header id="page-id">
				<h1>Sorry, we couldn't find what you are looking for</h1>
			</header>
		</div>
	</div>
</section>
<div class="row last">
<section role="main" class="large-9 columns">
<h2 class="alt-h2">Try searching for it!</h2>
<?php get_search_form(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
<article>
	<p><?php echo stripslashes(get_option('ranklab_404')); ?></p>
</article>
<?php endwhile; endif; ?>
</section>
<?php get_sidebar(); ?>
</div> <!-- #main -->
<?php get_footer(); ?>
