<?php get_header(); ?>
<div class="main clearfix">
<header id="page-message" class="clearfix">
	<div class="row">
	<div class="text-block small-12 columns">
		<h1>Our Blog</h1>
		<h2>
		<?php
			if ( get_post_meta($post->ID,'ranklab_page_message', true) ) {
				echo '- '.get_post_meta($post->ID,'ranklab_page_message', true);
			}
		?>
		</h2>	
	</div><!-- end text-block -->
</div><!-- end row -->
</header>
<section role="main" class="">
<div id="cat-block" class="row">
	<div class="small-12 columns">
	<h3>Topics:</h3>
	<ul>
		<?php wp_list_categories('orderby=name&title_li='); ?> 
	</ul>
	</div>
</div><!-- end cat-block -->

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<article class="blog">
		<div class="post-block">
			<div class="post-info">
				<div class="row">
				<div class="left-block large-6 columns">
					<p class="categories"><?php _e(  ); the_category(', '); // Separated by commas ?></p>
					<h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
					<div class="post-meta">
					 	<div class="author-block">
						 	<div class="avatar-block">
								<?php echo get_avatar( get_the_author_meta( 'ID' ), 50 ); ?>
							</div><!-- end avatar-block -->
							<div class="author-title"><?php the_author(); ?></div>
							<div class="post-date"><?php the_time('F jS Y') ?></div>
							<div class="post-comment"><?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></div><!-- end post-comment -->
					 	</div><!-- end author-block -->
					</div><!-- end post-meta -->
				</div><!-- end left-block -->
				
				<div class="right-block large-6 columns">
					<p><?php the_excerpt();?></p>
					<a href="<?php the_permalink();?>" class="">Keep Reading</a>
				</div><!-- end right-block -->
			</div><!-- end post-info -->
			</div><!-- end row -->
		</div><!-- end post-block -->
	</article><!-- end blog -->	
<?php endwhile; endif; ?>
<?php ranklab_pagination();?>
<?php echo paginate_links(
	'base'         => '%_%',
	'format'       => '?page=%#%',
	'total'        => 1,
	'current'      => 0,
	'show_all'     => False,
	'end_size'     => 1,
	'mid_size'     => 2,
	'prev_next'    => True,
	'prev_text'    => __('« Previous'),
	'next_text'    => __('Next »'),
	'type'         => 'plain',
	'add_args'     => False,
	'add_fragment' => '',
	'before_page_number' => '',
	'after_page_number' => ''
); ?>
<?php wp_reset_postdata(); ?>
</section>
</div> <!-- #main -->
<?php get_footer(); ?>