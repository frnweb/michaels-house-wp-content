  		<div id="footer">
	
  		<div class="row">
        <div id="connect_block" class="small-16 columns">
        <ul class="large-block-grid-5 medium-block-grid-3 small-block-grid-2">
  			<li>
  				<a href="http://www.michaelshouse.com" id="mh_logo">Michael's House Drug and Alcohol Treatment</a>
        </li>
        <li>
            <div id="talk_now">
  					<span class="talk_title">Talk to Someone Now</span>
  					<span class="talk_num" ><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Footer"]'); ?></span>
  				  </div>
        </li>
        <li>
  				<a href="http://www.foundationsrecoverynetwork.com/" id="foundations_logo" target="_blank" title="Foundations Recovery Network">Foundations Recovery Network</a>
        </li>
        <li>
  				<a href="http://www.carf.org/" target="_blank" title="CARF" id="carf_logo">CARF</a>
        </li>
        <li>
    				<div id="connect">	
    					<p>Connect With Us</p>
    					<ul>
    						<li><a href="http://www.facebook.com/michaelshousepalmsprings" class="icon_facebook" target="_blank">Facebook</a></li>
    						<li><a href="http://twitter.com/michaelshouse" class="icon_twitter" target="_blank">Twitter</a></li>
    						<li><a href="http://www.youtube.com/MichaelsHouseCA" class="icon_youtube" target="_blank">YouTube</a></li>
    					</ul>
  				  </div>
        </li>
        </ul>
        </div>
      </div>
  		<div class="row">
        <div class="small-16 columns">
  			<div id="link_block">
          <ul class="medium-block-grid-5 small-block-grid-2">
            <li>
        			<div class="widget widget_nav_menu">
        			<h3>Explore</h3>
        			<ul class="menu">
        				<li><a href="/">Home</a></li>
        				<li><a href="/about/">About</a></li>
        				<li><a href="/treatment-program/">Programs</a></li>
        				<li><a href="/treatment-admissions/">Admissions</a></li>
        				<li><a href="<?php echo do_shortcode('[frn_privacy_url]'); ?>" target="_blank">Privacy</a></li>
        				<li><a href="/terms/">Terms</a></li>
        				<li><a href="/sitemap/">Sitemap</a></li>
        			</ul>
        			</div>
			     </li>
			     <li>
            <div id="nav_menu-4" class="widget widget_nav_menu">
      			<h3>Research &amp; Learn</h3>
      			<ul class="menu">
      				<li><a href="/alcohol-rehab/alcoholism/">Alcoholism</a></li>
      				<li><a href="/cocaine-addiction/">Cocaine Addiction</a></li>
      				<li><a href="/substance-abuse/">Substance Abuse</a></li>
      				<li><a href="/drug-addiction/">Drug Addiction</a></li>
      				<li><a href="/featured-articles/">Featured Articles</a></li>
      				<li><a href="/articles/">All Articles</a></li>
      			</ul>
      			</div>
					</li>
          <li>
      			<div class="widget widget_nav_menu">
      			<h3>Heal With Us</h3>
      			<ul class="menu">
      				<li><a href="/drug-rehab/">Drug Rehab</a></li>
      				<li><a href="/alcohol-rehab/">Alcohol Rehab</a></li>
      				<li><a href="/alcohol-treatment/">Alcohol Treatment</a></li>
      				<li><a href="/detox/">Detox Services</a></li>
      				<li><a href="/drug-treatment/">Drug Treatment</a></li>
      				<li><a href="/drug-rehabilitation/">Drug Rehabilitation</a></li>
      			</ul>
      			</div>
          </li>
					<li>
      			<div class="widget widget_nav_menu">
      			<h3>Connect &amp; Share</h3>
      			<ul class="menu">
      			<li><a href="/blog/">Michael&#8217;s House Blog</a></li>
      			<li><a href="/contact/">Contact Us</a></li>
      			</ul>
      			</div>
          </li>   
          <li>
      			<div class="widget widget_nav_menu">
      			<h3>Meet Our Network</h3>
      			<ul class="menu">
      				<li><a href="http://www.dualdiagnosis.org" target="_blank">Dual Diagnosis Treatment</a></li>
      				<li><a href="http://www.thecyn.com" target="_blank">The Canyon Rehab Center</a></li>
      				<li><a href="http://www.lapalomatreatment.com" target="_blank">La Paloma Treatment Center</a></li>
      				<li><a href="http://www.foundationsatlanta.com" target="_blank">Foundations Atlanta</a></li>
      			</ul>
      			</div>	
          </li>    	
			  </ul>		
			</div><!-- end link_block -->  			
      </div>
    </div>
  			
  		<div class="row">
        <div class="small-16 columns">
  			<div id="sub_block">
  				<div id="copyright" class="left">
  					<p><?php echo do_shortcode('[frn_footer paragraph="no"]'); ?> <?php if (is_user_logged_in()) { ?> 
  					    (<?php echo get_num_queries(); ?> queries in <?php timer_stop(1); ?> seconds)
  					<?php } ?></p>
  					<ul>
  						<!--<li><a href="">Sitemap</a></li>-->
  					</ul>
				
  				</div>
          <div class="right"><a href="http://www.bbb.org/los-angeles/business-reviews/alcoholism-info-and-treatment-centers/michaels-house-in-palm-springs-ca-13049282" target="_blank"><img src="http://www.bbb.org/los-angeles/images/2/cbbb-badge-horz.png" WIDTH="135" HEIGHT="52" BORDER="0"></a></div>
        </div><!-- end copyright div -->
  			</div><!-- end sub_block -->
        </div>
  		</div>  
  		</div><!-- end footer div -->
  		
  		
  		
  	</div><!-- end wrapper div -->
  </div><!-- end main_content -->
  
 
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/library/js/scripts.js"></script>

  <?php	wp_footer(); ?>

<!-- Start Visual Website Optimizer Asynchronous Code -->
<script type='text/javascript'>
var _vwo_code=(function(){
var account_id=31005,
settings_tolerance=2000,
library_tolerance=1500,
use_existing_jquery=false,
// DO NOT EDIT BELOW THIS LINE
f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
</script>
<!-- End Visual Website Optimizer Asynchronous Code -->

<!-- Piwik added by Matt -->
<script type="text/javascript"> 
  var _paq = _paq || [];
  _paq.push(['trackPageView']);
  _paq.push(['trackSiteSearch', 
  "Banana", // Search keyword searched for 
  "Organic Food", // Search category selected in your search engine. If you do not need this, set to false  
   0 // Number of results on the Search results page. Zero indicates a 'No Result Search Keyword'. Set to false if you don't know 
   ]);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u=(("https:" == document.location.protocol) ? "https" : "http") + "://analytics.foundationsrecoverynetwork.com//";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', 1]);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript';
    g.defer=true; g.async=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();

</script>
<noscript><p><img src="http://analytics.foundationsrecoverynetwork.com/piwik.php?idsite=1" style="border:0" alt="" /></p></noscript>
<!-- End Piwik Code added by Matt -->

  </body>
</html>