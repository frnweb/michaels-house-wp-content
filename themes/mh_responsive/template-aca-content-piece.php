<?php
/*
Template Name: Obamacare
*/
?>
<?php get_header(); ?>


    <link rel="stylesheet" href="http://www.michaelshouse.com/wp-content/themes/mh_responsive/obamacare/css/foundation.css" />
    <link rel="stylesheet" href="http://www.michaelshouse.com/wp-content/themes/mh_responsive/obamacare/css/mh-obamacare-style.css" />
    <link rel="stylesheet" href="http://www.michaelshouse.com/wp-content/themes/mh_responsive/obamacare/css/animate.css">
    
  
    <!--Typekit Futura-->
    <script type="text/javascript">
    (function(d) {
      var config = {
        kitId: 'bee2msv',
        scriptTimeout: 3000
      },
      h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='//use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
    })(document);
  </script>

  <body>
<article id="obamacare">
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=208930139145155";
  fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
  

    <!---TITLE TIER -->
    <div class="light">
      <div class="row">
        <div class="large-12 columns large-centered">
          <h1 class="text-center">can obamacare change the face of addiction treatment?</h1>
        </div>
        <div class="small-16 columns">
          <ul class="inline-list right" style="margin-bottom: 0; padding-top: 1em;">
            <li><div id="facebook_like">
          <div class="fb-share-button" data-type="button_count"></div>
          </div>
          </li>
            <li><div id="twitter_share">
          <a href="http://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
          </div></li>
          <li><div class="plus-one-top">
          <g:plusone></g:plusone>
          <script type="text/javascript">
            (function() {
              var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
              po.src = 'https://apis.google.com/js/plusone.js';
              var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
            })();
          </script>
          </div>  
          </li>
          </ul>
        </div>
      </div>
    </div>
    <!---/end TITLE TIER -->
    <!--ADDCITION TREATMENT IN STATE OF NEGLIGENCE TIER -->
    <div class="dark">
      <div class="row less-pad">
        <div class="large-4 columns">
          <p class="arrow_fam">Prior to the transformations the Affordable Care Act [ACA] put in play, people who had addictions often struggled to pay for the services they might need in order to recover. These statistics released by the Substance Abuse and Mental Health Services Administration [SAMHSA] in 2012 <sup><a href="#source1">1</a></sup> tell that story quite well.</p>
        </div>
        <div class="large-12 columns">
          <h2 class="text-center alt-h2">Addiction Treatment is in a State of Negligence</h2>
          <ul class="large-block-grid-5 small-block-grid-1 wow pulse">
            <li class="text-center connected">
              <div class="circle-stat text-center">23.1<br/>Million</div>
              <p class="stat-blurb">Number of people 12 and older who needed care for illicit drug  addiction</p>
            </li>
            <li class="text-center connected">
              <div class="circle-stat text-center">20.6<br/>Million</div>
              <p class="stat-blurb">Those who needed care but did not get it in a specialty facility</p>
            </li>
            <li class="text-center connected">
              <div class="circle-stat text-center">1.1<br/>Million</div>
              <p class="stat-blurb">Persons felt they needed treatment for their illicit drug or alcohol use problem</p>
            </li>
            <li class="text-center connected">
              <div class="circle-stat percent text-center">68.7%</div>
              <p class="stat-blurb">Those who didn't get care because they didn't know where to go for help</p>
            </li>
            <li class="text-center connected">
              <div class="circle-stat percent text-center">38.2%</div>
              <p class="stat-blurb">Those who did not get care due to a lack of health care coverage</p>
            </li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="large-8 columns">
          <blockquote class="wow slideInLeft">Insurance companies need to understand that this is a disease… Heroin is life-threatening, I don’t care what they say. Because we're losing kids every day from it.<cite>Patty DiRenzo, Salvatore's Mother <sup><a href="#source2">2</a></sup></cite></blockquote>
        </div>
        <div class="large-8 columns">
          <p>When people don't get the care they need for an addiction that's in progress, the results can be heartbreaking. A family profiled in a story produced by the Associated Press demonstrates that tragedy. This family attempted to obtain inpatient care for heroin addiction for a young man for five years, and at each step of the way, they were met with difficulties, including lack of space and an unwillingness on the part of treatment facility administrators to provide residential care, which the family thought was the best method of treatment. At the end of this period, the young addict lost his life to an overdose.</p>
        </div>
      </div>
    </div>
    <!--/end ADDCITION TREATMENT IN STATE OF NEGLIGENCE TIER -->
    <!--RISKY BUSINESS TIER -->
    <div class="light">
      <div class="row">
        <h2 class="section-title text-center">Risky Business: outpatient is the standard of care</h2>
        <div class="small-16 columns">
          <p class="text-center">Prior to the health care law, most people got care in outpatient facilities, according to SAMHSA <sup><a href="#source3">3</a></sup>. The following chart shows a snapshot of the median number of patients that were being treated by various types of facilities across the US on March 30, 2012.</p>
        </div>
        <div class="small-16 columns">
          <h3 class="infograph text-center">Median number of patients</h3>
          <div class="progress small-13 secondary wow slideInLeft">
            <span class="meter" style="width: 42%">Regular Outpatient</span>
          </div>
          <div class="progress small-9 secondary dimmed wow slideInLeft">
            <span class="meter" style="width: 61%">Intensive Outpatient</span>
          </div>
          <div class="progress small-6 secondary dimmed wow slideInLeft">
            <span class="meter" style="width: 91%">Outpatient Day Treatment</span>
          </div>
          <div class="progress small-10 secondary wow slideInLeft">
            <span class="meter" style="width: 55%">Long-term Residential</span>
          </div>
          <div class="progress small-8 secondary dimmed wow slideInLeft">
            <span class="meter" style="width: 69%">Short-term Residential</span>
          </div>
          <div class="progress small-7 secondary dimmed wow slideInLeft">
            <span class="meter" style="width: 79%">Hospital Inpatient Treatment</span>
          </div>
          <div class="class small-11 small-push-5 columns">
            <ul class="small-block-grid-7 number-blocks">
              <li class="connected-numbers"><span class="statline">0</span></li>
              <li class="connected-numbers"><span class="statline">5</span></li>
              <li class="connected-numbers"><span class="statline">10</span></li>
              <li class="connected-numbers"><span class="statline">15</span></li>
              <li class="connected-numbers"><span class="statline">20</span></li>
              <li class="connected-numbers"><span class="statline">25</span></li>
              <li class="connected-numbers"><span class="statline">30</span></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!--/end RISKY BUSINESS TIER -->
    <!--INPATIENT TIER -->
    <div class="dark residential">
      <div class="row">
        <div class="large-8 columns">
          <blockquote class="wow pulse">
            Statistically, those who dedicate more than 30-days at an <span class="highlight">inpatient program</span> nearly double their rate of success for long-term sobriety, and are not nearly as prone to relapse.<cite>U-T San Diego <sup><a href="#source4">4</a></sup></cite>
          </blockquote>
        </div>
        <div class="large-8 columns">
          <p>It's quite possible that the new law will allow people to get care in residential facilities, if their doctors think this is the right kind of treatment for them. After all, doctors can work directly with insurance companies and push for specific types of care, and this is the kind of power the average consumer might not have. But it remains to be seen how the law might impact where services are provided.</p>
          <p>In fact, it's hard to provide firm statistics about how the ACA will impact any aspect of addiction care, as the law has only been in effect for a few months as of this writing, and hitches in the system might have prevented some people from signing up for the health care law at all.</p>
        </div>
      </div>
    </div>
    <div class="light less-pad">
      <div class="row">
        <div class="small-16 columns">
          <p class="arrow-orange">However, the last time the laws concerning health care and addictions were revised, due to the Mental Health Parity and Addiction Equality Act, there were huge changes seen in how addictions were treated and who got help for addictions. For example, according to a report published by the Health Care Cost Institute <sup><a href="#source5">5</a></sup>, admissions for a substance abuse problem in people younger than 65 rose 19.5 percent after parity was passed, and that jump was accompanied by an <span class="highlight">increase in spending on mental health care.</span> It's likely we'll see the same sorts of increases when the ACA is in full effect.</p>
        </div>
      </div>
    </div>
    <!--/end INPATIENT TIER -->
    <!--Millions will get treatment --> 
    <div class="dark less-pad">
      <div class="row">
        <div id="impact" class="small-16 columns">
          <h3 id="impact-title" class="infograph text-center">the impact: millons of people will get the treatment they need<br>
          <small class="dark-grey">the u.s. department of health and human services <sup><a href="#source6">6</a></sup> suggests that the aca has huge potential to provide care</small></h3>
          <ul class="medium-block-grid-4 hide-for-small">
            <li class="text-center"><img class="wow fadeInUp" src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/obamacare/img/27mil.png"></li>
            <li class="text-center"><img class="wow fadeInUp" src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/obamacare/img/3-9mil.png"></li>
            <li class="text-center"><img class="wow fadeInUp" src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/obamacare/img/7-1mil.png"></li>
            <li class="text-center"><img class="wow fadeInUp" src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/obamacare/img/24-5mil.png"></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="blue-bg less-pad">
      <div class="row">
        <div class="small-16 columns">
          <ul class="medium-block-grid-4 small-block-grid-1">
            <li class="million-blurb text-center"><img class="show-for-small-only" src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/obamacare/img/27mil.png"><br>27 Million People with no insurance will have coverage through the ACA</li>
            <li class="million-blurb"><img class="show-for-small-only" src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/obamacare/img/3-9mil.png"><br>3.9 Million people who have individual insurance will have access to new ACA plans that provide substance abuse treatment coverage, which was excluded from old plans</li>
            <li class="million-blurb"><img class="show-for-small-only" src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/obamacare/img/7-1mil.png"><br>7.1 million people in individual plans will benefit from federal parity protections regarding treatment caps and benefits through the ACA</li>
            <li class="million-blurb"><img class="show-for-small-only" src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/obamacare/img/24-5mil.png"><br>24.5 Million People in small group plans will benefit from parity protections in the ACA</li>
          </ul>
        </div>
      </div>
    </div>
    <!--/end Millions will get treatment --> 
    <!--Redistribution --> 
    <div class="light">
      <div class="row">
        <div class="small-16 columns">
          <h3 class="infograph text-center left-arrow-fam">redistribution of health: not all addicts are created equal</h3>
        </div>
        <div class="large-6 columns">
          <p>However, there are some limits to the benefits people might receive, and many of those limits involve availability. Treatment programs in many states have been downsized or eliminated altogether due to budget cuts and a lack of willingness to pay for treatment programs that might help. As a result, people who might have new insurance coverage that would assist with an addiction problem might discover that it's difficult to find a treatment program with available space. </p>
        </div>
        <div class="large-10 columns">
          <p>In addition, some older laws are in direct conflict with those in the ACA, and that might also impede treatment. For example, reports suggest <sup><a href="#source7">7</a></sup> that federal legislation that applies to treatment centers with more than <span class="highlight">16 beds</span> limits payments for treatments provided to low-income adults through the Medicaid program. People who applied for ACA coverage and who were enrolled in Medicaid due to low-income status might find that they can't get residential care, as Medicaid won't allow their facilities to pay for care. These sorts of kinks simply must be ironed out for the law to reach its full potential.</p>
          <img class="centered" src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/obamacare/img/beds.png">
        </div>
      </div>
    </div>
    <!--/end Redistribution --> 
    <!--BUYER BEWARE --> 
    <div class="blue-bg">
      <div class="row">
        <div class="small-16 columns">
          <h2 class="section-title white text-center">BUYER beware: the controversy of standardized care</h2>
          <p class="white">The ACA defines addiction therapies as "essential," meaning that they can't be denied outright or excluded from insurance policies. Since the law also mandates that everyone has insurance, it's reasonable to suggest that addiction treatments would be as routine as treatments given for heart disease or diabetes. Unfortunately, the law also has quite a bit of bend and flex, in terms of what is covered and what is not, and some consumers might be caught up in that flex. </p>
        </div>
      </div>
    </div>
    <div class="dark">
      <div class="row">
        <div class="small-16 columns">
          <p class="blue">For example, addictions are often combated with a cocktail of medications that can soothe cravings and ease withdrawal. For many people, these medications are considered absolutely vital, as the subtle tweak in chemistry allows them to go through an average day without feeling constant pressure to dip back into substance use and abuse. However, many other people are able to recover from addiction without using medications at all. As a result, <span class="highlight">some insurance programs simply don't provide coverage for addiction-related medications at all.</span>  Plans like this are required to provide an "exceptions process," meaning that patients can petition to get their excluded drugs covered by the insurance program, but that final ruling is still left up to the insurance program administrator.</p>
        </div>
      </div>
      <div class="row">
        <div class="medium-16 columns">
        <div class="medium-6 columns right">
          <img src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/obamacare/img/prescription_bottles.png">
        </div>
          <p class="blue">In addition, almost every single plan offered through the ACA has some form of "cost sharing." This means that people who enroll in these plans are required to make copayments for specific types of services, each time they use them, and sometimes there's no cap on the amount of money people might be required to pay. Prescription medications rarely apply to a deductible cap, for example, and some medications might cost a user <span class="highlight">$1,000 per month or more, according to reports <sup><a href="#source8">8</a></sup>.</span></p>
      </div>
    
        <div class="small-16 columns">
          <p class="blue"><span class="highlight">The same sorts of limitations might apply to the types of treatments people can get with their ACA plans.</span> For example, insurance companies might provide a list of "essential services" that would be covered in response to an addiction. Sometimes, these limitations can apply to the therapies the person might get in a treatment program. Psychotherapies might be covered, for example, but not innovative or alternative therapies like yoga or art therapy, even if they seem to be helpful. </p>
          
            <div class="medium-6 columns left">
            <img src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/obamacare/img/plans.png">
          </div>
          <p class="blue">
            There may also be issues of cost, regardless of insurance coverage. Plans available through the ACA exchanges are ranked by metal, with bronze providing the lowest level of coverage and gold providing a high level of care. Bronze plans might be cheaper on a monthly basis, but those plans require users to make significant copayments for each treatment they receive, and in some cases, <span class="highlight">the plans require users to spend a significant amount of money out of pocket before they get any benefits at all.</span> Gold plans don't tend to have upfront limits, and their copayments tend to be smaller, but there are still out-of-pocket payments associated with these plans. 
          </p>
        </div>
      </div>
    </div>
    <!--/end BUYER BEWARE -->
    <!--A Life of Addiction -->
    <div class="light">
      <div class="row">
        <div class="small-16 columns">
          <h2 class="section-title text-center left-arrow-fam">a life of addiction is still not worth it</h2>
          <p>While ACA plans might have some limitations, consumers are still encouraged to sign up for care. Even if they're required to make copayments and deal with deductibles, they might be eligible for tax subsidies that help them to pay their monthly premiums, and the amount they'll spend with insurance is likely much less than the amount they'd be required to pay if they had no insurance at all. </p>
          <p>Enrolling in the plans is relatively easy. Consumers can visit the exchanges run by their states by searching for the name of the state and "Affordable Care Act" online, or consumers can simply head to the <a href="https://www.healthcare.gov/" target="blank">Healthcare.gov</a> website and enroll there. Technically, there is only one open enrollment period (November to January), but some consumers dealing with complex situations or a difficult life event can apply anytime. </p>
          <h3 class="infograph text-center"><small class="blue">A typical enrollment form, whether on a state exchange or Heathcare.gov, requires consumers to provide information about:</small></h3>
          <ul class="small-block-grid-1 medium-block-grid-2 large-block-grid-4" data-equalizer>
            <li class="subgraphic"><div data-equalizer-watch><img src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/obamacare/img/household.png"></div><br>The age of all household 
members who need coverage</li>
            <li class="subgraphic"><div data-equalizer-watch><img src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/obamacare/img/tobacco.png"></div><br>Tobacco use among all 
enrollees in the household </li>
            <li class="subgraphic"><div data-equalizer-watch><img src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/obamacare/img/house-income.png"></div><br>Household income level
for the year prior</li>
            <li class="subgraphic"><div data-equalizer-watch><img src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/obamacare/img/income-level.png"></div><br>Projected income level
for the year to come</li>
          </ul>
        </div>
      </div>
    </div>
    <!--/end A Life of Addiction -->
    <!--What if you can't afford a plan -->
    <div class="dark">
      <div class="row">
        <div class="small-16 columns">
          <h2 class="section-title text-center">what if you can’t afford a plan?</h2>
          <p>The information above is used to help officials determine how much financial assistance a family might qualify for in order to pay for health care coverage. In some cases, the financial health of a family is so dire that they're enrolled in Medicaid programs provided by the state. When that happens, consumers have little to no control over what sorts of programs are covered and how much they'll be required to pay. There are no real levels of Medicaid; one-size-fits-all is the rule. But other consumers will be required to choose:</p>
          <ul class="small-block-grid-1 medium-block-grid-2 large-block-grid-4">
            <li class="subgraphic">A level of Coverage<br><img src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/obamacare/img/coverage.png"></li>
            <li class="subgraphic">An Insurance Provider<br><img src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/obamacare/img/insurance.png"></li>
            <li class="subgraphic">A start date for coverage<br><img src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/obamacare/img/date.png"></li>
            <li class="subgraphic">How much monthly assistance to accept<br><img src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/obamacare/img/assistance.png"><br><span class="subthin">(some choose to take their tax credits in one lump sum at the end of the year)</span></li>
          </ul>
          <div class="large-6 medium-8 columns">
            <p class="arrow_fam_end">Once that information is provided, the insurance plan the consumer chooses is notified, the enrollment in that program is completed, and the consumer receives an insurance card and formal enrollment paperwork in the mail, along with bills for service.</p>
          </div>
          <div class="large-10 medium-8 columns">
            <p>This process can be complicated, particularly for consumers who aren't familiar with standardized insurance terms and coverage particulars. Thankfully, many states provide brokers or other assistance that can explain all of the terms seen in a standardized plan, and they might be able to help consumers choose a plan that covers the sorts of therapies that might be included in a standardized addiction treatment program. </p>
          </div>
        </div>
      </div>
    </div>
    <!--/end What if you can't afford a plan -->
    <!--Victory? -->
    <!--/end Victory? -->
    <div class="blue-bg">
      <div class="row">
        <div class="small-16 columns">
          <h2 class="section-title white text-center">Is this a victory for families?</h2>
          <p class="white">As mentioned, many of the ACA plans don't cover all of the costs involved with treating an addiction in a comprehensive manner. However, consumers who are choosy when looking at plans, and who really attempt to find the right provider and the right plan, might be able to achieve the sort of comprehensive coverage that allows them to deal with an addiction without losing their financial future in the process. As a result, it's fairly safe to say that one of the big winners of the ACA is a person who has an addiction. </p>
        </div>
      </div>
    </div>
    <div class="dark">
      <div class="row">
        <div class="large-16 columns">
          <h4 id="blue-dot">Or is this just a step in the right direction?</h4>
          <div class="large-8 columns medium-4 small-16 right">
            <img src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/obamacare/img/medical.png">
          </div>
          <p>Addiction professionals, including therapists, might also benefit from ACA protections, however, as their work begins to attain a glow of widespread social respectability. Instead of providing treatments that may have previously seemed somehow superfluous and unnecessary to those who didn’t know much about addiction treatment, they're providing care that has the seal of approval from a governmental agency. </p>
          <p>But costs are likely to remain high, and they're also likely to vary from state to state, especially when medications are taken into account. For example, according to research published in Health Affairs <sup><a href="#source9">9</a></sup>, payments for prescription drugs account for about two-thirds of all out-of-pocket expenses related to mental health or addiction treatment, and people who live in the South and the Midwest pay more than those living in the West or the Northeast. Sad statistics like this seem to suggest that drug companies might be the real beneficiaries of the ACA law, and that consumers in some places might still be priced out of the care they need unless additional restrictions are put into place. </p>
          <p class="last-line">Insurance doesn't cover each and every treatment at the 100 percent level. And often, people who have longstanding issues with addiction have a reduced capacity to pay for basics like food and shelter, much less covering the costs of monthly insurance premiums and hefty copayments. If these people can pull the money together, they might still find it difficult to find treatment programs that have the space to accept them, and they might find that their insurance companies won't cover the kind of care people think is best, due to cost-control measures. </p>
          <p class="last">While the ACA is certainly a step in the right direction for people who have addictions, much work remains to be done before people touched by substance abuse have the help they'll need in order to keep their chronic condition under control. </p>
        </div>
      </div>
    </div>
    <div class="light" style="padding-bottom:20em;">
      <div class="row">
        <div class="small-16 columns">
          <h4 class="text-center">Resources</h4>
          <ol class="no-bullet">
            <li><a name="source1" href="http://www.samhsa.gov/data/NSDUH/2012SummNatFindDetTables/NationalFindings/NSDUHresults2012.pdf" target="blank">SAMHSA: Results from the 2012 National Survey on Drug Use and Health: Summary of National Findings </a></li>
            <li><a name ="source3" href="http://www.samhsa.gov/data/DASIS/NSSATS2012_Web.pdf">National Survey of Substance Abuse Treatment Services (N-SSATS): 2012 Data on Substance Abuse Treatment Facilities</a> </li>
            <li><a name ="source4" href="http://addiction.utsandiego.com/articles/substance-abuse-therapy-inpatient-vs-outpatient-therapy/">Substance Abuse Therapy: Inpatient vs Outpatient Therapy, UT San Diego</a></li>
            <li><a name ="source5" href="http://www.healthcostinstitute.org/news-and-events/press-release-impact-mental-health-parity-and-addiction-equity-act-inpatient-admissi">HCCI: Mental Health-Substance Use Services In Hospitals Up After Parity Law, Finds New Report</a></li>
            <li><a name ="source6" href="http://aspe.hhs.gov/health/reports/2013/mental/rb_mental.cfm">ASPE: Affordable Care Act Expands Mental Health and Substance Use Disorder Benefits and Federal Parity Protections for 62 Million Americans</a></li>
            <li><a name ="source7" href="http://www.wtsp.com">Despite Obamacare, a gaping hole in addiction treatment</a></li>
            <li><a name ="source8" href="http://chronicle.northcoastnow.com/2014/04/07/heroin-addicts-face-barriers-treatment/">AP: Heroin addicts face barriers to treatment</a></li>
            <li><a name ="source9" href="http://content.healthaffairs.org/content/28/3/713.full">Health Affairs: State Variations In The Out-Of-Pocket Spending Burden For Outpatient Mental Health Treatment</a></li>
          </0l>
        </div>
      </div>
    </div>
  </article>  
  <?php get_footer(); ?>
  <script src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/obamacare/js/vendor/jquery.js"></script>
    <script src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/obamacare/js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
</script>
  <script src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/obamacare/js/wow.min.js"></script>
  <script>
   new WOW().init();
  </script>
  </body>
</html>