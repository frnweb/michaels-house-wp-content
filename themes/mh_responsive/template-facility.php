<?php
/*
Template Name: Facility
*/
?>


<?php get_header(); ?>
<section class="banner facility">
	
	<h1 class="text-center"><?php the_title(); ?><br><small><?php echo get_post_meta(get_the_id(), 'blurb', true); ?></small></h1>
	<div class="row">
		<div class="small-12 columns">
			<?php echo get_post_meta(get_the_id(), 'image-tour', true); ?>
		</div>
	</div>
	<?php include get_template_directory().'/social.php'; ?>
</section>

<section role="main" class="row">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
	<?php the_content(); ?>
	<?php endwhile; endif; ?>
</section>
<section class="light-block last spacing">
	<div class="row">
		<div class="small-12 columns">
			<div class="text-center">
				<a href="http://www.michaelshouse.com/treatment-admissions" class="button large round secondary"><span class="step">Step 4</span><span class="step-text">What Does the Admissions Process Look Like?</span></a>
				<p class="helper">Learn all you need to know through a few easy steps</p>
			</div>	
		</div>
	</div>
</section>
<?php get_footer(); ?>