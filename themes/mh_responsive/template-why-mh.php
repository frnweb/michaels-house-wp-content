<?php
/*
Template Name: Why Michael's House
*/
?>


<?php get_header(); ?>
<section class="banner why-mh">
	<h1 class="text-center">Why People Choose Us<br><small>Trusted by Patients and Families Since 1989</small></h1>
	<?php include get_template_directory().'/social.php'; ?>
</section>
<section class="spacing">
	<div class="row bottom-spacing" data-equalizer>
		<div class="large-6 columns" data-equalizer-watch>
			<h2 class="text-center">
				25 Years of Evidence-Based Treatment
			</h2>
			<p>
				When Arlene Rosen lost her son Michael to a drug overdose, she started a treatment center so that she could help spare other families from the pain she experienced. Michael's House opened in 1989 and has been serving patients and their families through comprehensive integrated treatment for over two decades.
			</p>
			<p>
				In 2006, Foundations Recovery Network added Michael's House to its family of treatment centers, supporting Arlene's vision and expanding the center to accommodate more patients. Once a facility with 24 beds, Michael's House now has 120 beds&mdash;and has helped countless men and women find the path to long-term recovery from addiction and mental health conditions.
			</p>
			<div class="text-center">
				<a href="http://www.michaelshouse.com/treatment-program" class="button large round secondary"><span class="step">Step 2</span><span class="step-text">What are my treatment options?</span></a>
				<p class="helper">Learn all you need to know through a few easy steps</p>
			</div>	
		</div>
		<div class="large-6 medium-8 medium-pull-2 large-reset-order columns" data-equalizer-watch>
			<div class="flex-video">
				<iframe width="1280" height="720" src="//www.youtube.com/embed/kIgdewIktVs" frameborder="0" allowfullscreen></iframe>
			</div>
			<ul class="large-block-grid-4 medium-block-grid-4 small-block-grid-4 accredited-bar" style="height: 65px;">
				<li class="orange">Accredited<br>Treatment</li>
				<li><img src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/style/images/badge.png"></li>
				<li><img src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/style/images/caarf.png"></li>
				<li><img src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/style/images/naatp.png"></li>
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="small-12 columns">
			<blockquote>“We tried lots of other programs. He had case workers, doctors, and they would send him to other programs. All of which failed. It was fortunate he finally made it to Michael's House."</blockquote>
		</div>
	</div>
</section>
<section class="light-block spacing">
	<div class="row">
		<div class="small-12 columns">
		    <ul class="tabs" data-tab>
			  <li class="tab-title active"><a href="#panel2-1">Foundations Treatment Model</a></li>
			  <li class="tab-title"><a href="#panel2-2">Dialectical Behavior Therapy</a></li>
			  <li class="tab-title"><a href="#panel2-3">Comprehensive Treatment</a></li>
			  <li class="tab-title"><a href="#panel2-4">Proven Success</a></li>
			</ul>
			<div class="tabs-content">
			  <div class="content active" id="panel2-1">
			    <p>Our treatment model utilizes Motivational Interviewing methods and philosophies and recognizes Stages of Change in the recovery process. We emphasize concurrent treatment for substance abuse and co-occurring mental health disorders. Our evidence-based, integrative methods have been proven more effective than traditional drug and alcohol treatment methods in a five-year research study using the Foundations Treatment Model. At Michael’s House we help people restore their lives by embracing a way of life based upon the 12-Step principles of Alcoholics Anonymous (AA), Narcotics Anonymous (NA) and Dual Recovery Anonymous (DRA).</p>
			  </div>
			  <div class="content" id="panel2-2">
			    <p>At its core Dialectical Behavior Therapy (DBT) is designed to help people understand and adjust their thoughts and emotional responses. In this innovative treatment style, the individual works closely with a cognitive therapist to develop real-world strategies for dealing with the issues they face during recovery from alcohol or drug addiction.  With DBT, the individual is able to build their self-esteem, discover how to resolve conflict, and generally enjoying a healthier, more fulfilling life.</p>
			  </div>
			  <div class="content" id="panel2-3">
			    <p>Many	<a href="http://www.michaelshouse.com/drug-rehab">drug rehab</a> and <a href="http://www.michaelshouse.com/alcohol-rehab">alcohol rehab</a> programs offer treatment that deals with addiction but not the other emotional and mental issues that the majority of individuals with substance abuse issues are also facing. Consider the following fact: Up to 65.5% of individuals with substance abuse/dependency issues have at least one co-occurring mental disorder, and 51% of individuals with a mental disorder also have at least one co-occurring addictive disorder (Kessler, 1994). Michael’s House understands that everyone who enters the program brings their own unique set of circumstances -which is why the counselors and staff provide individualized care that treats the entire person in mind, body and spirit.</p>
			  </div>
			  <div class="content" id="panel2-4">
			    <p>Michael’s House offers treatment for addictive disorders and co-occurring mental disorders together. Our evidence-based, integrative methods have been proven more effective than traditional drug and <a href="http://www.michaelshouse.com/alcohol-treatment/">alcohol treatment</a> methods in a five-year research study using the Foundations Treatment Model.</p>
			  </div>
			</div>			
		</div> 
	</div>
</section>
<section class="spacing last">
	<div class="row" data-equalizer>
		<h2 class="text-center alt-h2">Most Insurance Accepted</h2>
		<div class="large-8 medium-6 columns text-center insurance-logos" data-equalizer-watch>
			<img src="http://www.michaelshouse.com/wp-content/themes/mh_responsive/style/images/insurance-logos.jpg" alt="accepted insurance logos">
		</div>
		<div class="large-4 medium-6 columns text-center" data-equalizer-watch>
			<div class="vert-center">
				<a href="http://www.michaelshouse.com/insurance" class="button large radius"><span class="arrow">Check if you're covered</span></a>
				<p class="orange">We can help you navigate insurance coverage</p>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>