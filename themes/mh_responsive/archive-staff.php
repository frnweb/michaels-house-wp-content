<?php get_header(); ?>
<section class="light-block standard-header">
  <div class="row">
    <div class="small-12 columns">
      <header id="page-id">
        <h1>Staff</h1>
        <?php get_template_part('library/includes/breadcrumbs'); ?>
        <?php include get_template_directory().'/social.php'; ?>
      </header>
    </div>
  </div>
</section>

<section class="border-block">
<div class="row">
   <div class="large-7 large-centered medium-8 medium-centered small-12 columns text-center">
    <h2>CEO and Medical Director</h2>
    <ul class="medium-block-grid-2 small-block-grid-1">
      <?php $the_query = new WP_Query( 'post_type=staff&posts_per_page=6&staff_location=executive' );
          while ( $the_query->have_posts() ) : $the_query->the_post();?>
          <li>
            <div class="headshot">
              <a href="<?php echo the_permalink(); ?>"><?php echo get_the_post_thumbnail();?></a>             
            </div>
            <h3 class="text-center"><small><strong><?php echo get_the_title(); ?></strong><br> 
            <?php echo get_post_meta(get_the_ID(),'frothy_staff_role', true); ?></small></h3>
          </li>
        <?php endwhile; wp_reset_postdata(); ?>       
    </ul>
  </div>
</div>
</section>

<section class="border-block">
  <div class="row">
    <div class="large-6 large-centered medium-8 medium-centered small-12 columns text-center">
      <h2>Clinical Managers</h2>
      <ul class="medium-block-grid-2 small-block-grid-1">
      <?php $the_query = new WP_Query( 'post_type=staff&posts_per_page=6&staff_location=clinical-managers' );
          while ( $the_query->have_posts() ) : $the_query->the_post();?>
          <li>
            <div class="headshot">
              <a href="<?php echo the_permalink(); ?>"><?php echo get_the_post_thumbnail();?></a>             
            </div>
            <h3 class="text-center"><small><strong><?php echo get_the_title(); ?></strong><br> 
            <?php echo get_post_meta(get_the_ID(),'frothy_staff_role', true); ?></small></h3>
          </li>
        <?php endwhile; wp_reset_postdata(); ?>
      </ul>
    </div>
  </div>
</section>
<section class="border-block">
  <div class="row">
    <div class="small-12 columns text-center">
      <h2>Clinical Staff</h2>
      <ul class="large-block-grid-6 medium-block-grid-4 small-block-grid-1">
        <?php $the_query = new WP_Query( 'post_type=staff&posts_per_page=50&staff_location=clinical-staff' );
          while ( $the_query->have_posts() ) : $the_query->the_post();?>
          <li>
            <div class="headshot">
              <a href="<?php echo the_permalink(); ?>"><?php echo get_the_post_thumbnail();?></a>             
            </div>
            <h3 class="text-center"><small><strong><?php echo get_the_title(); ?></strong><br> 
            <?php echo get_post_meta(get_the_ID(),'frothy_staff_role', true); ?></small></h3>
          </li>
        <?php endwhile; wp_reset_postdata(); ?>
      </ul>
    </div>
  </div>
</section>
<section class="border-block">
  <div class="row">
    <div class="large-6 large-centered medium-10 medium-centered small-12 columns text-center">
      <h2>Medical Support Staff</h2> 
      <ul class="medium-block-grid-2 small-block-grid-1">           
        <?php $the_query = new WP_Query( 'post_type=staff&posts_per_page=10&staff_location=medical-support' );
          while ( $the_query->have_posts() ) : $the_query->the_post();?>
          <li>
            <div class="headshot">
              <a href="<?php echo the_permalink(); ?>"><?php echo get_the_post_thumbnail();?></a>             
            </div>
            <h3 class="text-center"><small><strong><?php echo get_the_title(); ?></strong><br> 
            <?php echo get_post_meta(get_the_ID(),'frothy_staff_role', true); ?></small></h3>
          </li>
        <?php endwhile; wp_reset_postdata(); ?>
      </ul>
    </div>
  </div>
</section>
<section class="border-block last">
  <div class="row">
    <div class="large-10 large-centered small-12 columns text-center">
      <h2>Facility Operations</h2>
      <ul class="medium-block-grid-4 small-block-grid-1">
        <?php $the_query = new WP_Query( 'post_type=staff&posts_per_page=10&staff_location=facility-operations' );
          while ( $the_query->have_posts() ) : $the_query->the_post();?>
          <li>
            <div class="headshot">
              <a href="<?php echo the_permalink(); ?>"><?php echo get_the_post_thumbnail();?></a>             
            </div>
            <h3 class="text-center"><small><strong><?php echo get_the_title(); ?></strong><br> 
            <?php echo get_post_meta(get_the_ID(),'frothy_staff_role', true); ?></small></h3>
          </li>
        <?php endwhile; wp_reset_postdata(); ?>
      </ul>
    </div>
  </div>
</section>
<?php get_footer(); ?>