<?php get_header(); ?>
<section class="light-block standard-header">
	<div class="row">
		<div class="small-12 columns">
			<header id="page-id">
				<h1><?php the_title(); ?></h1>
				<?php get_template_part('library/includes/breadcrumbs'); ?>
			</header>
		</div>
	</div>
	<?php include get_template_directory().'/social.php'; ?>
</section>
<div class="row">
<section role="main" class="large-9 columns">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
<article>
	<!-- post details -->
	<div class="postmeta">
		<p class="date"><?php the_time('F j, Y'); ?></p>
		<p class="tags"><?php the_tags( __( 'Tags: ' ), ', ', '<br>'); // Separated by commas with a line break at the end ?></p>			
		<p class="categories"><?php _e( 'Categorized in: ' ); the_category(', '); // Separated by commas ?></p>		
	</div><!-- end postmeta -->
	<!-- /post details -->
	
	<!-- post thumbnail -->
	<?php
	if ( has_post_thumbnail() ) {
		the_post_thumbnail( 'large' );
	}
	?>
	<!-- end post thumbnail -->
	<?php the_content(); ?>
</article>
<?php endwhile; endif; ?>
</section>
<?php get_sidebar(); ?>
</div>
<div class="row last">
	<div class="panel small-12 columns">
		<p><em>Articles posted here are primarily educational and may not directly reflect the offerings at Michael’s House. For more specific information on programs at Michael’s House, contact us today.</em></p>
	</div>
</div>
 <!-- #main -->
<?php get_footer(); ?>